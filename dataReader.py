import pandas as pd
import numpy as np

class dataReader():
  def __init__(self, name, dataPath, ws):
    self.__name = name
    self.__dataPath = dataPath
    self.__worksheet = ws
    self.__df, self.__df2 = self.readFile()
    return
  def name(self):
    return self.__name
  def dataPath(self):
    return self.__dataPath
  def worksheet(self):
    return self.__worksheet
  def readFile(self):
    xls = pd.ExcelFile(self.__dataPath)
    df = pd.read_excel(xls, self.__worksheet)
    df.columns = list(df.iloc[0]) # Reset header to column content
    df = df.drop([0]) # Drop repeated row since already set to header
    df = df.reset_index(drop=True) # Reset indexing
    df2 = None
    if 'VgsIdRampData' in self.__worksheet:
      df = df.drop(columns=['Time [s]']) # Drop unnecessary columns
      df = df[57:438]
      df = df.iloc[:,:4*20+1]
      df = df.reset_index(drop=True) # Reset indexing
    if 'OffStateData' in self.__worksheet:
      df = df[:27]
      df = df.iloc[:,:4*20+1]
    if 'OnStateData' in self.__worksheet:
      df2 = df[29:]
      df2 = df2.reset_index(drop=True)
      df2 = df2.iloc[:,:4*20+1]
      df = df[:26]
      df = df.iloc[:,:4*20+1]
    # Rename header
    prefix = ''
    starting = int(self.__worksheet.split('-')[1])-19
    newNames = []
    for i in range(len(df.columns)):
      prefix = 'ganfet ' + str((i // 4) + starting) + ' '
      newNames.append(prefix + df.columns[i])
    df.columns = newNames
    try:
      df2.columns = newNames
    except:
      pass
    return (df, df2)
  def DF(self):
    return self.__df, self.__df2
