import numpy as np
import ganfet
import matplotlib.pyplot as plt
from scipy.interpolate import make_interp_spline
import statistics
import atlas_mpl_style as ampl
import os

class merger():
  
  def __init__(self, batchType, ganfets):
    self.__batchType = batchType
    self.__ganfets = ganfets

  def ganfets(self):
    return self.__ganfets
  
  def plotVgsIdRamp(self, largeFont=False):
    # Activate ATLAS style
    ampl.use_atlas_style()
  
    # Gather ganfets data
    fileName = 'merged{}VgsIdRamp'.format(self.__batchType.replace("-",""))
    xsPre = []
    ysPre = []
    xsPost = []
    ysPost = []
    ratios = []
    for fet in self.__ganfets:
      prefix = 'ganfet '+str(fet.num())+' '
      xPre = fet.vgsIdRampPreIrrad()[prefix+'VGS'].to_numpy()
      yPre = fet.vgsIdRampPreIrrad()[prefix+'IDS'].to_numpy()
      xPost = fet.vgsIdRampPostIrrad()[prefix+'VGS'].to_numpy()
      yPost = fet.vgsIdRampPostIrrad()[prefix+'IDS'].to_numpy()
      xsPre.append(xPre)
      ysPre.append(yPre)
      xsPost.append(xPost)
      ysPost.append(yPost)

    # Plot
    for i in range(len(xsPre)):
      if i == 0:
        plt.plot(xsPre[i], ysPre[i], color='black', marker='.', markersize=3, alpha = 0.3, label="Pre Gamma Irradiation")
        plt.plot(xsPost[i], ysPost[i], color='red', marker='.', markersize=3, alpha = 0.3, label="Post Gamma Irradiation")
      else:
        plt.plot(xsPre[i], ysPre[i], color='black', marker='.', markersize=3, alpha = 0.3)
        plt.plot(xsPost[i], ysPost[i], color='red', marker='.', markersize=3, alpha = 0.3)

    # Plot editing
    plt.xlim([0,3.5])
    plt.ylim([-0.1,2.2])
    xmin, xmax, ymin, ymax = plt.axis()
    if largeFont:
      plt.ylim([-0.1,3.4])
      xmin, xmax, ymin, ymax = plt.axis()
      plt.xlabel(r'$\mathregular{V_{GS}}$ [V]', fontsize=22)
      plt.ylabel(r'$\mathregular{I_{D}}$ [A]', fontsize=22)
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.9+ymin, "ATLAS ITk", fontstyle='italic', fontweight='bold', fontsize=22)
      plt.text((xmax-xmin)*0.24+xmin, (ymax-ymin)*0.9+ymin, "Strip", fontstyle='italic', fontsize=22)
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.83+ymin, "{} Batch Sample".format(self.__batchType), fontsize=22)
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.76+ymin, "Data from 80/80 GaNFETs", fontsize=22)
      plt.legend(loc=(0.03, 0.59), ncol=1, numpoints=5, fontsize=22).get_frame().set_linewidth(0.0)
      plt.yticks(fontsize=22)
      plt.xticks(fontsize=22)
    else:
      plt.xlabel(r'$\mathregular{V_{GS}}$ [V]')
      plt.ylabel(r'$\mathregular{I_{D}}$ [A]')
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.9+ymin, "ATLAS ITk", fontstyle='italic', fontweight='bold')
      plt.text((xmax-xmin)*0.205+xmin, (ymax-ymin)*0.9+ymin, "Strip", fontstyle='italic')
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.84+ymin,"Data from 80/80 GaNFETs")
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.84+ymin, "{} Batch Sample".format(self.__batchType))
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.78+ymin, "Data from 80/80 GaNFETs")
      plt.legend(loc=(0.03, 0.64), ncol=1, numpoints=5).get_frame().set_linewidth(0.0)

    # Save figure
    try:
      plt.savefig('./../data/plots/'+fileName+".png", format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)
    except:
      os.system("mkdir ./../data/plots")
      plt.savefig('./../data/plots/'+fileName+".png", format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)

    # Close plot
    plt.close()

  def plotIdAtVgs0VFromVgsIdRamp(self, largeFont=False):
    # Activate ATLAS style
    ampl.use_atlas_style()

    # General variables
    fileName = "merged{}VgsIdRampIdAtVgs0V".format(self.__batchType.replace("-",""))

    # Get relevant ganfet variables
    idsAtVgs0VPre = []
    idsAtVgs0VPost = []
    for fet in self.__ganfets:
      prefix = 'ganfet '+str(fet.num())+' '
      yPre = fet.vgsIdRampPreIrrad()[prefix+'IDS'].to_numpy()[0]
      yPost = fet.vgsIdRampPostIrrad()[prefix+'IDS'].to_numpy()[0]
      idsAtVgs0VPre.append(yPre*1e3)
      idsAtVgs0VPost.append(yPost*1e3)

    # Get distribution statistics
    preMean = statistics.mean(idsAtVgs0VPre)
    preStdev = statistics.stdev(idsAtVgs0VPre)
    postMean = statistics.mean(idsAtVgs0VPost)
    postStdev = statistics.stdev(idsAtVgs0VPost)

    # Plot
    if self.__batchType == 'Production':
      plt.hist(idsAtVgs0VPre, bins=50, range=(-7.5, 30), color='black', alpha=0.5, label='Pre Gamma Irradiation')
      plt.hist(idsAtVgs0VPost, bins=50, range=(-7.5, 30), color='red', alpha=0.5, label='Post Gamma Irradiation')
    else:
      plt.hist(idsAtVgs0VPre, bins=50, range=(-11.5, 11.5), color='black', alpha=0.5, label='Pre Gamma Irradiation')
      plt.hist(idsAtVgs0VPost, bins=50, range=(-11.5, 11.5), color='red', alpha=0.5, label='Post Gamma Irradiation')

    # Plot editing
    if self.__batchType == 'Production':
      plt.xlim([-7.5,30])
    else:
      plt.xlim([-11.5,11.5])
    xmin, xmax, ymin, ymax = plt.axis()
    if largeFont:
      plt.xlabel(r'$\mathregular{I_{D}}$ at $\mathregular{V_{GS}=0\,V}$ [mA]', fontsize=22)
      plt.ylabel(r'Number of GaNFETs', fontsize=22)
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.9+ymin, "ATLAS ITk", fontstyle='italic', fontweight='bold', fontsize=22)
      plt.text((xmax-xmin)*0.24+xmin, (ymax-ymin)*0.9+ymin, "Strip", fontstyle='italic', fontsize=22)
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.83+ymin, "{} Batch Sample".format(self.__batchType), fontsize=22)
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.76+ymin, "Data from 80/80 GaNFETs", fontsize=22)
      plt.legend(loc=(0.04, 0.59), ncol=1, numpoints=5, fontsize=22).get_frame().set_linewidth(0.0)
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.545+ymin, r"$\mathregular{(\overline{x}\,\pm\,\sigma)_{pre}}$" + r" = ({:.2f}".format(preMean)+r"$\,\mathregular{\pm}\,$"+"{:.2f})$\,$mA".format(preStdev), fontsize=22)
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.475+ymin, r"$\mathregular{(\overline{x}\,\pm\,\sigma)_{post}}$" + r" = ({:.2f}".format(postMean)+r"$\,\mathregular{\pm}\,$"+"{:.2f})$\,$mA".format(postStdev), fontsize=22)
      plt.yticks(fontsize=22) 
      plt.xticks(fontsize=22)
    else:
      plt.xlabel(r'$\mathregular{I_{D}}$ at $\mathregular{V_{GS}=0\,V}$ [mA]')
      plt.ylabel(r'Number of GaNFETs')
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.90+ymin, "ATLAS ITk", fontstyle='italic', fontweight='bold')
      plt.text((xmax-xmin)*0.205+xmin, (ymax-ymin)*0.9+ymin, "Strip", fontstyle='italic')
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.84+ymin, "{} Batch Sample".format(self.__batchType))
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.78+ymin, "Data from 80/80 GaNFETs")
      plt.legend(loc=(0.04, 0.64), ncol=1, numpoints=5).get_frame().set_linewidth(0.0)
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.605+ymin, r"$\mathregular{(\overline{x}\,\pm\,\sigma)_{pre}}$" + r" = ({:.2f}".format(preMean)+r"$\,\mathregular{\pm}\,$"+"{:.2f})$\,$mA".format(preStdev))
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.545+ymin, r"$\mathregular{(\overline{x}\,\pm\,\sigma)_{post}}$" + r" = ({:.2f}".format(postMean)+r"$\,\mathregular{\pm}\,$"+"{:.2f})$\,$mA".format(postStdev))

    # Save figure
    try:
      plt.savefig('./../data/plots/'+fileName+".png", format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)
    except:
      os.system("mkdir ./../data/plots")
      plt.savefig('./../data/plots/'+fileName+".png", format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)

    # Close plot
    plt.close()

  def plotIdShiftAtVgs0VFromVgsIdRamp(self, largeFont=False):
    # Activate ATLAS style
    ampl.use_atlas_style()

    # General variables
    fileName = "merged{}VgsIdRampIdShiftAtVgs0V".format(self.__batchType.replace("-",""))

    # Get relevant ganfet variables
    idShiftsAtVgs0V = []
    for fet in self.__ganfets:
      prefix = 'ganfet '+str(fet.num())+' '
      yPre = fet.vgsIdRampPreIrrad()[prefix+'IDS'].to_numpy()[0]
      yPost = fet.vgsIdRampPostIrrad()[prefix+'IDS'].to_numpy()[0]
      idShiftsAtVgs0V.append((yPost-yPre)*1e3)

    # Get distribution statistics
    mean = statistics.mean(idShiftsAtVgs0V)
    stdev = statistics.stdev(idShiftsAtVgs0V)

    # Plot
    if self.__batchType == 'Production':
      plt.hist(idShiftsAtVgs0V, bins=20, range=(2,30), color='blue', alpha=0.5)
    else:
      plt.hist(idShiftsAtVgs0V, bins=20, range=(-23,2), color='blue', alpha=0.5)

    # Plot editing
    if self.__batchType == 'Production':
      plt.xlim([2,30])
    else:
      plt.xlim([-23,2])
    xmin, xmax, ymin, ymax = plt.axis()
    if largeFont:
      plt.xlabel(r'$\mathregular{I_{D,post}-I_{D,pre}}$ at $\mathregular{V_{GS}=0.0\,V}$ [mA]', fontsize=22)
      plt.ylabel(r'Number of GaNFETs', fontsize=22)
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.9+ymin, "ATLAS ITk", fontstyle='italic', fontweight='bold', fontsize=22)
      plt.text((xmax-xmin)*0.24+xmin, (ymax-ymin)*0.9+ymin, "Strip", fontstyle='italic', fontsize=22)
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.83+ymin, "{} Batch Sample".format(self.__batchType), fontsize=22)
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.76+ymin, "Data from 80/80 GaNFETs", fontsize=22)
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.69+ymin, r"$\mathregular{\overline{x}\,\pm\,\sigma}$"+r" = ({:.2f}".format(mean)+r"$\,\mathregular{\pm}\,$"+r"{:.2f})$\,$mA".format(stdev), fontsize=22)
      plt.yticks(fontsize=22)
      plt.xticks(fontsize=22)
    else:
      plt.xlabel(r'$\mathregular{I_{D,post}-I_{D,pre}}$ at $\mathregular{V_{GS}=0.0\,V}$ [mA]')
      plt.ylabel(r'Number of GaNFETs')
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.90+ymin, "ATLAS ITk", fontstyle='italic', fontweight='bold')
      plt.text((xmax-xmin)*0.205+xmin, (ymax-ymin)*0.9+ymin, "Strip", fontstyle='italic')
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.84+ymin, "{} Batch Sample".format(self.__batchType), fontsize=22)
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.78+ymin, "Data from 80/80 GaNFETs", fontsize=22)
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.72+ymin, r"$\mathregular{\overline{x}\,\pm\,\sigma}$"+r" = ({:.2f}".format(mean)+r"$\,\mathregular{\pm}\,$"+r"{:.2f})$\,$mA".format(stdev))

    # Save figure
    try:
      plt.savefig('./../data/plots/'+fileName+".png", format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)
    except:
      os.system("mkdir ./../data/plots")
      plt.savefig('./../data/plots/'+fileName+".png", format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)

    # Close plot
    plt.close()

  def plotIdAtVgs2p5VFromVgsIdRamp(self, largeFont=False):
    # Activate ATLAS style
    ampl.use_atlas_style()

    # General variables
    fileName = "merged{}VgsIdRampIdAtVgs2p5V".format(self.__batchType.replace("-",""))

    # Get relevant ganfet variables
    idsAtVgs2p5VPre = []
    idsAtVgs2p5VPost = []
    for fet in self.__ganfets:
      prefix = 'ganfet '+str(fet.num())+' '
      yPre = fet.vgsIdRampPreIrrad()[prefix+'IDS'].to_numpy()[-1]
      yPost = fet.vgsIdRampPostIrrad()[prefix+'IDS'].to_numpy()[-1]
      idsAtVgs2p5VPre.append(yPre)
      idsAtVgs2p5VPost.append(yPost)

    # Get distribution statistics
    preMean = statistics.mean(idsAtVgs2p5VPre)
    preStdev = statistics.stdev(idsAtVgs2p5VPre)
    postMean = statistics.mean(idsAtVgs2p5VPost)
    postStdev = statistics.stdev(idsAtVgs2p5VPost)

    # Plot
    if self.__batchType == 'Production':
      plt.hist(idsAtVgs2p5VPre, bins=50, range=(1.872,1.91), color='black', alpha=0.5, label='Pre Gamma Irradiation')
      plt.hist(idsAtVgs2p5VPost, bins=50, range=(1.872,1.91), color='red', alpha=0.5, label='Post Gamma Irradiation')
    else:
      plt.hist(idsAtVgs2p5VPre, bins=50, range=(1.946,1.973), color='black', alpha=0.5, label='Pre Gamma Irradiation')
      plt.hist(idsAtVgs2p5VPost, bins=50, range=(1.946,1.973), color='red', alpha=0.5, label='Post Gamma Irradiation')

    # Plot editing
    if self.__batchType == 'Production':
      xShift = 0.0
      plt.xlim([1.872,1.91])
    else:
      xShift = 0.43
      plt.xlim([1.946,1.973])
    xmin, xmax, ymin, ymax = plt.axis()
    if largeFont:
      plt.xlabel(r'$\mathregular{I_{D}}$ at $\mathregular{V_{GS}=2.5\,V}$ [A]', fontsize=22)
      plt.ylabel(r'Number of GaNFETs', fontsize=22)
      plt.text((xmax-xmin)*(0.05+xShift)+xmin, (ymax-ymin)*0.9+ymin, "ATLAS ITk", fontstyle='italic', fontweight='bold', fontsize=22)
      plt.text((xmax-xmin)*(0.24+xShift)+xmin, (ymax-ymin)*0.9+ymin, "Strip", fontstyle='italic', fontsize=22)
      plt.text((xmax-xmin)*(0.05+xShift)+xmin, (ymax-ymin)*0.83+ymin, "{} Batch Sample".format(self.__batchType), fontsize=22)
      plt.text((xmax-xmin)*(0.05+xShift)+xmin, (ymax-ymin)*0.76+ymin, "Data from 80/80 GaNFETs", fontsize=22)
      plt.legend(loc=(0.04+xShift, 0.59), ncol=1, numpoints=5, fontsize=22).get_frame().set_linewidth(0.0)
      plt.text((xmax-xmin)*(0.05+xShift)+xmin, (ymax-ymin)*0.545+ymin, r"$\mathregular{(\overline{x}\,\pm\,\sigma)_{pre}}$"+r" = ({:.3f}".format(preMean)+r"$\,\mathregular{\pm}\,$"+r"{:.3f})$\,$A".format(preStdev), fontsize=22)
      plt.text((xmax-xmin)*(0.05+xShift)+xmin, (ymax-ymin)*0.475+ymin, r"$\mathregular{(\overline{x}\,\pm\,\sigma)_{post}}$"+r" = ({:.3f}".format(postMean)+r"$\,\mathregular{\pm}\,$"+r"{:.3f})$\,$A".format(postStdev), fontsize=22)
      plt.yticks(fontsize=22)
      plt.xticks(fontsize=22)
    else:
      plt.xlabel(r'$\mathregular{I_{D}}$ at $\mathregular{V_{GS}=2.5\,V}$ [A]')
      plt.ylabel(r'Number of GaNFETs')
      plt.text((xmax-xmin)*(0.05+xShift)+xmin, (ymax-ymin)*0.90+ymin, "ATLAS ITk", fontstyle='italic', fontweight='bold')
      plt.text((xmax-xmin)*(0.205+xShift)+xmin, (ymax-ymin)*0.9+ymin, "Strip", fontstyle='italic')
      plt.text((xmax-xmin)*(0.05+xShift)+xmin, (ymax-ymin)*0.84+ymin, "{} Batch Sample".format(self.__batchType), fontsize=22)
      plt.text((xmax-xmin)*(0.05+xShift)+xmin, (ymax-ymin)*0.78+ymin, "Data from 80/80 GaNFETs", fontsize=22)
      plt.legend(loc=(0.04+xShift, 0.64), ncol=1, numpoints=5).get_frame().set_linewidth(0.0)
      plt.text((xmax-xmin)*(0.05+xShift)+xmin, (ymax-ymin)*0.605+ymin, r"$\mathregular{(\overline{x}\,\pm\,\sigma)_{pre}}$"+r" = ({:.3f}".format(preMean)+r"$\,\mathregular{\pm}\,$"+r"{:.3f})$\,$A".format(preStdev))
      plt.text((xmax-xmin)*(0.05+xShift)+xmin, (ymax-ymin)*0.545+ymin, r"$\mathregular{(\overline{x}\,\pm\,\sigma)_{post}}$"+r" = ({:.3f}".format(postMean)+r"$\,\mathregular{\pm}\,$"+r"{:.3f})$\,$A".format(postStdev))

    # Save figure
    try:
      plt.savefig('./../data/plots/'+fileName+".png", format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)
    except:
      os.system("mkdir ./../data/plots")
      plt.savefig('./../data/plots/'+fileName+".png", format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)

    # Close plot
    plt.close()

  def plotIdShiftAtVgs2p5VFromVgsIdRamp(self, largeFont=False):
    # Activate ATLAS style
    ampl.use_atlas_style()

    # General variables
    fileName = "merged{}VgsIdRampIdShiftAtVgs2p5V".format(self.__batchType.replace("-",""))

    # Get relevant ganfet variables
    idShiftsAtVgs2p5V = []
    for fet in self.__ganfets:
      prefix = 'ganfet '+str(fet.num())+' '
      yPre = fet.vgsIdRampPreIrrad()[prefix+'IDS'].to_numpy()[-1]
      yPost = fet.vgsIdRampPostIrrad()[prefix+'IDS'].to_numpy()[-1]
      idShiftsAtVgs2p5V.append((yPost-yPre)*1e3)

    # Get distribution statistics
    mean = statistics.mean(idShiftsAtVgs2p5V)
    stdev = statistics.stdev(idShiftsAtVgs2p5V)

    # Plot
    if self.__batchType == 'Production':
      plt.hist(idShiftsAtVgs2p5V, bins=25, range=(-27,12), color='blue', alpha=0.5)
    else:
      plt.hist(idShiftsAtVgs2p5V, bins=25, range=(-17,7), color='blue', alpha=0.5)

    # Plot editing
    if self.__batchType == 'Production':
      plt.xlim([-27,12])
      xShift = 0.0
    else:
      plt.xlim([-17,7])
      xShift = 0.42
    xmin, xmax, ymin, ymax = plt.axis()
    if largeFont:
      plt.xlabel(r'$\mathregular{I_{D,post}-I_{D,pre}}$ at $\mathregular{V_{GS}=2.5\,V}$ [mA]', fontsize=22)
      plt.ylabel(r'Number of GaNFETs', fontsize=22)
      plt.text((xmax-xmin)*(0.05+xShift)+xmin, (ymax-ymin)*0.9+ymin, "ATLAS ITk", fontstyle='italic', fontweight='bold', fontsize=22)
      plt.text((xmax-xmin)*(0.24+xShift)+xmin, (ymax-ymin)*0.9+ymin, "Strip", fontstyle='italic', fontsize=22)
      plt.text((xmax-xmin)*(0.05+xShift)+xmin, (ymax-ymin)*0.83+ymin, "{} Batch Sample".format(self.__batchType), fontsize=22)
      plt.text((xmax-xmin)*(0.05+xShift)+xmin, (ymax-ymin)*0.76+ymin, "Data from 80/80 GaNFETs", fontsize=22)
      plt.text((xmax-xmin)*(0.05+xShift)+xmin, (ymax-ymin)*0.69+ymin, r"$\mathregular{\overline{x}\,\pm\,\sigma}}$"+r" = ({:.2f}".format(mean)+r"$\,\mathregular{\pm}\,$"+r"{:.2f})$\,$mA".format(stdev), fontsize=22)
      plt.yticks(fontsize=22)
      plt.xticks(fontsize=22)
    else:
      plt.xlabel(r'$\mathregular{I_{D,post}-I_{D,pre}}$ at $\mathregular{V_{GS}=2.5\,V}$ [mA]')
      plt.ylabel(r'Number of GaNFETs')
      plt.text((xmax-xmin)*(0.05+xShift)+xmin, (ymax-ymin)*0.90+ymin, "ATLAS ITk", fontstyle='italic', fontweight='bold')
      plt.text((xmax-xmin)*(0.205+xShift)+xmin, (ymax-ymin)*0.9+ymin, "Strip", fontstyle='italic')
      plt.text((xmax-xmin)*(0.05+xShift)+xmin, (ymax-ymin)*0.84+ymin, "{} Batch Sample".format(self.__batchType))
      plt.text((xmax-xmin)*(0.05+xShift)+xmin, (ymax-ymin)*0.78+ymin, "Data from 80/80 GaNFETs")
      plt.text((xmax-xmin)*(0.05+xShift)+xmin, (ymax-ymin)*0.72+ymin, r"$\mathregular{\overline{x}\,\pm\,\sigma}}$"+r" = ({:.2f}".format(mean)+r"$\,\mathregular{\pm}\,$"+r"{:.2f})$\,$mA".format(stdev))

    # Save figure
    try:
      plt.savefig('./../data/plots/'+fileName+".png", format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)
    except:
      os.system("mkdir ./../data/plots")
      plt.savefig('./../data/plots/'+fileName+".png", format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)

    # Close plot
    plt.close()

  def plotVoltageThsFromVgsIdRamp(self, largeFont=False):
    # Activate ATLAS style
    ampl.use_atlas_style()

    # General variables
    fileName = "merged{}VgsIdRampVoltageThs".format(self.__batchType.replace("-",""))

    # Get relevant ganfet variables
    vThsPre = []
    vThsPost = []
    for fet in self.__ganfets:
      vThPre, vThPost, vThShift = fet.voltageTh()
      vThsPre.append(vThPre)
      vThsPost.append(vThPost)

    # Get distribution statistics
    preMean = statistics.mean(vThsPre)
    preStdev = statistics.stdev(vThsPre)
    postMean = statistics.mean(vThsPost)
    postStdev = statistics.stdev(vThsPost)

    # Plot
    if self.__batchType == 'Production':
      plt.hist(vThsPre, bins=25, range=(1.53,1.8), color='black', alpha=0.5, label='Pre Gamma Irradiation')
      plt.hist(vThsPost, bins=25, range=(1.53,1.8), color='red', alpha=0.5, label='Post Gamma Irradiation')
    else:
      plt.hist(vThsPre, bins=25, range=(1.23,1.575), color='black', alpha=0.5, label='Pre Gamma Irradiation')
      plt.hist(vThsPost, bins=25, range=(1.23,1.575), color='red', alpha=0.5, label='Post Gamma Irradiation')

    # Plot editing
    if self.__batchType == 'Production':
      plt.xlim([1.53,1.8])
    else:
      plt.xlim([1.23,1.575])
    xmin, xmax, ymin, ymax = plt.axis()
    if largeFont:
      plt.xlabel(r'Voltage threshold [V]', fontsize=22)
      plt.ylabel(r'Number of GaNFETs', fontsize=22)
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.9+ymin, "ATLAS ITk", fontstyle='italic', fontweight='bold', fontsize=22)
      plt.text((xmax-xmin)*0.24+xmin, (ymax-ymin)*0.9+ymin, "Strip", fontstyle='italic', fontsize=22)
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.83+ymin, "{} Batch Sample".format(self.__batchType), fontsize=22)
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.76+ymin, "Data from 80/80 GaNFETs", fontsize=22)
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.69+ymin, r"$\mathregular{V_{GS}}$ at $\mathregular{I_{D}\,\approx\,0.95}\,A$", fontsize=22)
      plt.legend(loc=(0.04, 0.52), ncol=1, numpoints=5, fontsize=22).get_frame().set_linewidth(0.0)
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.475+ymin, r"$\mathregular{(\overline{x}\,\pm\,\sigma)_{pre}}}$"+r" = ({:.3f}".format(preMean)+r"$\,\mathregular{\pm}\,$"+r"{:.3f})$\,V$".format(preStdev), fontsize=22)
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.405+ymin, r"$\mathregular{(\overline{x}\,\pm\,\sigma)_{post}}}$"+r" = ({:.3f}".format(postMean)+r"$\,\mathregular{\pm}\,$"+r"{:.3f})$\,V$".format(postStdev), fontsize=22)
      plt.yticks(fontsize=22)
      plt.xticks(fontsize=22)
    else:
      plt.xlabel(r'Voltage threshold [V]')
      plt.ylabel(r'Number of GaNFETs')
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.90+ymin, "ATLAS ITk", fontstyle='italic', fontweight='bold')
      plt.text((xmax-xmin)*0.205+xmin, (ymax-ymin)*0.9+ymin, "Strip", fontstyle='italic')
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.84+ymin, "{} Batch Sample".format(self.__batchType))
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.78+ymin, "Data from 80/80 GaNFETs")
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.72+ymin, r"$\mathregular{V_{GS}}$ at $\mathregular{I_{D}\,\approx\,0.95}\,A$")
      plt.legend(loc=(0.04, 0.58), ncol=1, numpoints=5).get_frame().set_linewidth(0.0)
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.545+ymin, r"$\mathregular{(\overline{x}\,\pm\,\sigma)_{pre}}}$"+r" = ({:.3f}".format(preMean)+r"$\,\mathregular{\pm}\,$"+r"{:.3f})$\,V$".format(preStdev))
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.505+ymin, r"$\mathregular{(\overline{x}\,\pm\,\sigma)_{post}}}$"+r" = ({:.3f}".format(postMean)+r"$\,\mathregular{\pm}\,$"+r"{:.3f})$\,V$".format(postStdev))
    
    # Save figure
    try:
      plt.savefig('./../data/plots/'+fileName+".png", format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)
    except:
      os.system("mkdir ./../data/plots")
      plt.savefig('./../data/plots/'+fileName+".png", format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)

    # Close plot
    plt.close()

  def plotVoltageThsShiftFromVgsIdRamp(self, largeFont=False):
    # Activate ATLAS style
    ampl.use_atlas_style()

    # General variables
    fileName = "merged{}VgsIdRampVoltageThsShift".format(self.__batchType.replace("-",""))

    # Get relevant ganfet variables
    vThsShift = []
    for fet in self.__ganfets:
      vThPre, vThPost, vThShift = fet.voltageTh()
      vThsShift.append(vThShift*1e3)

    # Get distribution statistics
    mean = statistics.mean(vThsShift)
    stdev = statistics.stdev(vThsShift)

    # Plot
    if self.__batchType == 'Production':
      plt.hist(vThsShift, bins=30, range=(-110,50), color='blue', alpha=0.5)
    else:
      plt.hist(vThsShift, bins=30, range=(-110,110), color='blue', alpha=0.5)

    # Plot editing
    if self.__batchType == 'Production':
      plt.xlim([-110,50])
      xShift = 0.0
    else:
      plt.xlim([-110,110])
      xShift = -0.46
    xmin, xmax, ymin, ymax = plt.axis()
    if largeFont:
      plt.xlabel(r'$\mathregular{V_{GS,post}-V_{GS,pre}}$ [mV]', fontsize=22)
      plt.ylabel(r'Number of GaNFETs', fontsize=22)
      plt.text((xmax-xmin)*(0.51+xShift)+xmin, (ymax-ymin)*0.9+ymin, "ATLAS ITk", fontstyle='italic', fontweight='bold', fontsize=22)
      plt.text((xmax-xmin)*(0.70+xShift)+xmin, (ymax-ymin)*0.9+ymin, "Strip", fontstyle='italic', fontsize=22)
      plt.text((xmax-xmin)*(0.51+xShift)+xmin, (ymax-ymin)*0.83+ymin, "{} Batch Sample".format(self.__batchType), fontsize=22)
      plt.text((xmax-xmin)*(0.51+xShift)+xmin, (ymax-ymin)*0.76+ymin, "Data from 80/80 GaNFETs", fontsize=22)
      plt.text((xmax-xmin)*(0.51+xShift)+xmin, (ymax-ymin)*0.69+ymin, r"Voltage shift at $\mathregular{I_{D}\,\approx\,0.95}\,A$", fontsize=22)
      plt.text((xmax-xmin)*(0.51+xShift)+xmin, (ymax-ymin)*0.62+ymin, r"$\mathregular{\overline{x}\,\pm\,\sigma}$"+r" = ({:.2f}".format(mean)+r"$\,\mathregular{\pm}\,$"+r"{:.2f})$\,$mV".format(stdev), fontsize=22)
      plt.yticks(fontsize=22)
      plt.xticks(fontsize=22)
    else:
      plt.xlabel(r'$\mathregular{V_{GS,post}-V_{GS,pre}}$ [mV]')
      plt.ylabel(r'Number of GaNFETs')
      plt.text((xmax-xmin)*(0.57+xShift)+xmin, (ymax-ymin)*0.9+ymin, "ATLAS ITk", fontstyle='italic', fontweight='bold')
      plt.text((xmax-xmin)*(0.725+xShift)+xmin, (ymax-ymin)*0.9+ymin, "Strip", fontstyle='italic')
      plt.text((xmax-xmin)*(0.57+xShift)+xmin, (ymax-ymin)*0.84+ymin, "{} Batch Sample".format(self.__batchType))
      plt.text((xmax-xmin)*(0.57+xShift)+xmin, (ymax-ymin)*0.78+ymin, "Data from 80/80 GaNFETs")
      plt.text((xmax-xmin)*(0.57+xShift)+xmin, (ymax-ymin)*0.72+ymin, r"Voltage shift at $\mathregular{I_{D}\,\approx\,0.95}\,A$")
      plt.text((xmax-xmin)*(0.57+xShift)+xmin, (ymax-ymin)*0.66+ymin, r"$\mathregular{\overline{x}\,\pm\,\sigma}$"+r" = ({:.2f}".format(mean)+r"$\,\mathregular{\pm}\,$"+r"{:.2f})$\,$mV".format(stdev))

    # Save figure
    try:
      plt.savefig('./../data/plots/'+fileName+".png", format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)
    except:
      os.system("mkdir ./../data/plots")
      plt.savefig('./../data/plots/'+fileName+".png", format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)

    # Close plot
    plt.close()

  def plotVDSFromVgsIdRamp(self, largeFont=False):
    # Activate ATLAS style
    ampl.use_atlas_style()

    # General variables
    fileName = 'merged{}VgsIdRampVDS'.format(self.__batchType.replace("-",""))

    # Get relevant data
    xsPre = []
    ysPre = []
    xsPost = []
    ysPost = []
    for fet in self.__ganfets:
      prefix = 'ganfet '+str(fet.num())+' '
      # Get pre and post irradiation data
      xPre = fet.vgsIdRampPreIrrad()[prefix+'VGS'].to_numpy()
      yPre = fet.vgsIdRampPreIrrad()[prefix+'VDS'].to_numpy()
      xPost = fet.vgsIdRampPostIrrad()[prefix+'VGS'].to_numpy()
      yPost = fet.vgsIdRampPostIrrad()[prefix+'VDS'].to_numpy()
      xsPre.append(xPre)
      ysPre.append(yPre)
      xsPost.append(xPost)
      ysPost.append(yPost)

    # Plot
    for i in range(len(xsPre)):
      if i == 0:
        # Plot pre irradiation data
        plt.plot(xsPre[i], ysPre[i], color='black', marker='.', markersize=3, alpha = 0.3, label="Pre Gamma Irradiation")
        plt.plot(xsPost[i], ysPost[i], color='red', marker='.', markersize=3, alpha = 0.3, label="Post Gamma Irradiation")
      else:
        plt.plot(xsPre[i], ysPre[i], color='black', marker='.', markersize=3, alpha = 0.3)
        plt.plot(xsPost[i], ysPost[i], color='red', marker='.', markersize=3, alpha = 0.3)

    # Plot editing
    plt.xlim([0,3.5])
    plt.ylim([-0.5,11])
    xmin, xmax, ymin, ymax = plt.axis()
    if largeFont:
      plt.xlabel(r'$\mathregular{V_{GS}}$ [V]', fontsize=22)
      plt.ylabel(r'$\mathregular{V_{DS}}$ [V]', fontsize=22)
      plt.text((xmax-xmin)*0.5+xmin, (ymax-ymin)*0.9+ymin, "ATLAS ITk", fontstyle='italic', fontweight='bold', fontsize=22)
      plt.text((xmax-xmin)*0.69+xmin, (ymax-ymin)*0.9+ymin, "Strip", fontstyle='italic', fontsize=22)
      plt.text((xmax-xmin)*0.5+xmin, (ymax-ymin)*0.83+ymin, "{} Batch Sample".format(self.__batchType), fontsize=22)
      plt.text((xmax-xmin)*0.5+xmin, (ymax-ymin)*0.76+ymin, "Data from 80/80 GaNFETs", fontsize=22)
      plt.legend(loc=(0.48, 0.59), ncol=1, numpoints=5, fontsize=22).get_frame().set_linewidth(0.0)
      plt.yticks(fontsize=22)
      plt.xticks(fontsize=22)
    else:
      plt.xlabel(r'$\mathregular{V_{GS}}$ [V]')
      plt.ylabel(r'$\mathregular{V_{DS}}$ [V]')
      plt.text((xmax-xmin)*0.55+xmin, (ymax-ymin)*0.90+ymin, "ATLAS ITk", fontstyle='italic', fontweight='bold')
      plt.text((xmax-xmin)*0.705+xmin, (ymax-ymin)*0.9+ymin, "Strip", fontstyle='italic')
      plt.text((xmax-xmin)*0.55+xmin, (ymax-ymin)*0.84+ymin, "{} Batch Sample".format(self.__batchType))
      plt.text((xmax-xmin)*0.55+xmin, (ymax-ymin)*0.72+ymin, "Data from 80/80 GaNFETs")
      plt.legend(loc=(0.54, 0.56), ncol=1, numpoints=5).get_frame().set_linewidth(0.0)

    # Save figure
    try:
      plt.savefig('./../data/plots/'+fileName+".png", format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)
    except:
      os.system("mkdir ./../data/plots")
      plt.savefig('./../data/plots/'+fileName+".png", format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)

    # Close plot
    plt.close()

  def plotVsupplyFromVgsIdRamp(self, largeFont=False):
    # Activate ATLAS style
    ampl.use_atlas_style()
    
    # General variables
    fileName = 'merged{}VgsIdRampVsupply'.format(self.__batchType.replace("-",""))

    # Get data
    xsPre = []
    ysPre = []
    xsPost = []
    ysPost = []
    for fet in self.__ganfets:
      prefix = 'ganfet '+str(fet.num())+' '
      # Get pre and post irradiation data
      xPre = fet.vgsIdRampPreIrrad()[prefix+'VGS'].to_numpy()
      yPre = fet.vgsIdRampPreIrrad()[prefix+'Vsupply'].to_numpy()
      xPost = fet.vgsIdRampPostIrrad()[prefix+'VGS'].to_numpy()
      yPost = fet.vgsIdRampPostIrrad()[prefix+'Vsupply'].to_numpy()
      xsPre.append(xPre)
      ysPre.append(yPre)
      xsPost.append(xPost)
      ysPost.append(yPost)

    # Plot
    for i in range(len(xsPre)):
      if i == 0:
        plt.plot(xsPre[i], ysPre[i], color='black', marker='.', markersize=3, alpha = 0.3, label="Pre Gamma Irradiation")
        plt.plot(xsPost[i], ysPost[i], color='red', marker='.', markersize=3, alpha = 0.3, label="Post Gamma Irradiation")
      else:
        plt.plot(xsPre[i], ysPre[i], color='black', marker='.', markersize=3, alpha = 0.3)
        plt.plot(xsPost[i], ysPost[i], color='red', marker='.', markersize=3, alpha = 0.3)

    # Plot editing
    plt.xlim([0,3.5])
    plt.ylim([8.9,11])
    xmin, xmax, ymin, ymax = plt.axis()

    if largeFont:
      plt.xlabel(r'$\mathregular{V_{GS}}$ [V]', fontsize=22)
      plt.ylabel(r'$\mathregular{V_{supply}}$ [V]', fontsize=22)
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.9+ymin, "ATLAS ITk", fontstyle='italic', fontweight='bold', fontsize=22)
      plt.text((xmax-xmin)*0.24+xmin, (ymax-ymin)*0.9+ymin, "Strip", fontstyle='italic', fontsize=22)
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.83+ymin, "{} Batch Sample".format(self.__batchType), fontsize=22)
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.76+ymin, "Data from 80/80 GaNFETs", fontsize=22)
      plt.legend(loc=(0.03, 0.59), ncol=1, numpoints=5, fontsize=22).get_frame().set_linewidth(0.0)
      plt.yticks(fontsize=22)
      plt.xticks(fontsize=22)
    else:
      plt.xlabel(r'$\mathregular{V_{GS}}$ [V]')
      plt.ylabel(r'$\mathregular{V_{supply}}$ [V]')
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.90+ymin, "ATLAS ITk", fontstyle='italic', fontweight='bold')
      plt.text((xmax-xmin)*0.205+xmin, (ymax-ymin)*0.9+ymin, "Strip", fontstyle='italic')
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.84+ymin, "{} Batch Sample".format(self.__batchType))
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.72+ymin, "Data from 80/80 GaNFETs")
      plt.legend(loc=(0.04, 0.56), ncol=1, numpoints=5).get_frame().set_linewidth(0.0)

    # Save figure
    try:
      plt.savefig('./../data/plots/'+fileName+".png", format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)
    except:
      os.system("mkdir ./../data/plots")
      plt.savefig('./../data/plots/'+fileName+".png", format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)

    # Close plot
    plt.close()

  def plotOffState(self, largeFont=False, log=False):
    # Activate ATLAS style
    ampl.use_atlas_style()

    # General variables
    fileName = 'merged{}OffState'.format(self.__batchType.replace("-",""))

    # Get and plot data
    for fet in self.__ganfets:
      prefix = 'ganfet '+str(fet.num())+' '

      # Get pre and post irradiation data
      xPre = fet.offStatePreIrrad()[prefix+'VGS'].to_numpy()
      yPre = fet.offStatePreIrrad()[prefix+'ID'].to_numpy()*1e-3
      xPost = fet.offStatePostIrrad()[prefix+'VGS'].to_numpy()
      yPost = fet.offStatePostIrrad()[prefix+'ID'].to_numpy()*1e-3

      # Plot pre irradiation data
      if fet.num() == 1:
        plt.plot(xPre, yPre, color='black', marker='.', markersize=3, alpha = 0.3, label="Pre Gamma Irradiation")
        plt.plot(xPost, yPost, color='red', marker='.', markersize=3, alpha = 0.3, label="Post Gamma Irradiation")
      else:
        plt.plot(xPre, yPre, color='black', marker='.', markersize=3, alpha = 0.3)
        plt.plot(xPost, yPost, color='red', marker='.', markersize=3, alpha = 0.3)

    # Edit plotting
    plt.ticklabel_format(axis='y', style='sci', scilimits=(0,0))
    plt.xlim([0,600])
    if log and -999 not in xPre:
      plt.yscale("log")
      plt.ylim([3e-3,7e5])
    else:
      plt.ylim([-20,600])
    if largeFont:
      plt.xlabel(r'$\mathregular{V_{DS}}$ [V]', fontsize=22)
      plt.ylabel(r'$\mathregular{I_{D}}\ [nA]$', fontsize=22)
      plt.text(0.05, 0.9, "ATLAS ITk", fontstyle='italic', fontweight='bold', fontsize=22, transform=plt.gca().transAxes)
      plt.text(0.24, 0.9, "Strip", fontstyle='italic', fontsize=22, transform=plt.gca().transAxes)
      plt.text(0.05, 0.83, "{} Batch Sample".format(self.__batchType), fontsize=22, transform=plt.gca().transAxes)
      if self.__batchType == 'Production':
        plt.text(0.05, 0.76, "Pre data from 80/80 GaNFETs", fontsize=22, transform=plt.gca().transAxes)
        plt.text(0.05, 0.69, "Post data from 76/80 GaNFETs", fontsize=22, transform=plt.gca().transAxes)
        plt.legend(loc=(0.03, 0.52), ncol=1, numpoints=5, fontsize=22).get_frame().set_linewidth(0.0)
      else:
        plt.text(0.05, 0.76, "Data from 16/16 GaNFETs", fontsize=22, transform=plt.gca().transAxes)
        plt.legend(loc=(0.03, 0.59), ncol=1, numpoints=5, fontsize=22).get_frame().set_linewidth(0.0)
      plt.yticks(fontsize=22)
      plt.xticks(fontsize=22)
    else:
      plt.xlabel(r'$\mathregular{V_{DS}}$ [V]')
      plt.ylabel(r'$\mathregular{I_{D}}\ [nA]$')
      plt.text(0.05, 0.90, "ATLAS ITk", fontstyle='italic', fontweight='bold', transform=plt.gca().transAxes)
      plt.text(0.205, 0.90, "Strip", fontstyle='italic', transform=plt.gca().transAxes)
      plt.text(0.05, 0.84, "{} Batch Sample".format(self.__batchType), transform=plt.gca().transAxes)
      if self.__batchType == 'Production':
        plt.text(0.05, 0.78, "Pre data from 80/80 GaNFETs", transform=plt.gca().transAxes)
        plt.text(0.05, 0.72, "Post data from 76/80 GaNFETs", transform=plt.gca().transAxes)
        plt.legend(loc=(0.03, 0.58), ncol=1, numpoints=5).get_frame().set_linewidth(0.0)
      else:
        plt.text(0.05, 0.78, "Data from 16/16 GaNFETs", transform=plt.gca().transAxes)
        plt.legend(loc=(0.03, 0.64), ncol=1, numpoints=5).get_frame().set_linewidth(0.0)

    # Save figure
    try:
      if log:
        plt.savefig('./../data/plots/'+fileName+"Log.png", format='png', dpi=200)
        plt.savefig('./../data/plots/'+fileName+"Log.pdf", format='pdf', dpi=200)
        plt.savefig('./../data/plots/'+fileName+"Log.eps", format='eps', dpi=200)
      else:
        plt.savefig('./../data/plots/'+fileName+".png", format='png', dpi=200)
        plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
        plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)
    except:
      os.system("mkdir ./../data/plots")
      if log:
        plt.savefig('./../data/plots/'+fileName+"Log.png", format='png', dpi=200)
        plt.savefig('./../data/plots/'+fileName+"Log.pdf", format='pdf', dpi=200)
        plt.savefig('./../data/plots/'+fileName+"Log.eps", format='eps', dpi=200)
      else:
        plt.savefig('./../data/plots/'+fileName+".png", format='png', dpi=200)
        plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
        plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)

    # Close plot
    plt.close()
  
  def plotOffStateDerivative(self, largeFont=False, log=False):
    # Activate ATLAS style
    ampl.use_atlas_style()

    # General variables
    fileName = 'merged{}OffStateDerivative'.format(self.__batchType.replace("-",""))

    # Get and plot data
    for fet in self.__ganfets:
      prefix = 'ganfet '+str(fet.num())+' '

      # Get pre and post irradiation data
      xPre = fet.offStatePreIrrad()[prefix+'VGS'].to_numpy()
      yPre = fet.offStatePreIrrad()[prefix+'ID'].to_numpy()*1e-3
      xPost = fet.offStatePostIrrad()[prefix+'VGS'].to_numpy()
      yPost = fet.offStatePostIrrad()[prefix+'ID'].to_numpy()*1e-3

      # Calculate slopes between points
      xPre2 = [(xPre[i+1]+xPre[i])/2 for i in range(len(xPre)-1)]
      yPre2 = [(yPre[i+1]-yPre[i])/(xPre[i+1]-xPre[i]) if (xPre[i+1]-xPre[i]) > 0 else -999 for i in range(len(xPre)-1)]
      xPost2 = [(xPost[i+1]+xPost[i])/2 for i in range(len(xPost)-1)]
      yPost2 = [(yPost[i+1]-yPost[i])/(xPost[i+1]-xPost[i]) if (xPost[i+1]-xPost[i]) > 0 else -999 for i in range(len(xPost)-1)]
      
      # Plot pre irradiation data
      if fet.num() == 1:
        plt.plot(xPre2, yPre2, color='black', marker='.', markersize=3, alpha = 0.3, label="Pre Gamma Irradiation")
        plt.plot(xPost2, yPost2, color='red', marker='.', markersize=3, alpha = 0.3, label="Post Gamma Irradiation")
      else:
        plt.plot(xPre2, yPre2, color='black', marker='.', markersize=3, alpha = 0.3)
        plt.plot(xPost2, yPost2, color='red', marker='.', markersize=3, alpha = 0.3)

    # Edit plotting
    plt.ticklabel_format(axis='y', style='sci', scilimits=(0,0))
    plt.xlim([0,600])
    if log and -999 not in xPre:
      plt.yscale("log")
      plt.ylim([2e-3,1e2])
    else:
      plt.ylim([-0.25,4])
    if largeFont:
      plt.xlabel(r'$\mathregular{V_{DS}\ range\ (x\,\pm\,10V)}$ [V]', fontsize=22)
      plt.ylabel(r'$\mathregular{\frac{\Delta I_{D}}{\Delta V_{DS}}}\ \left[\frac{nA}{V}\right]$', fontsize=22)
      plt.text(0.05, 0.9, "ATLAS ITk", fontstyle='italic', fontweight='bold', fontsize=22, transform=plt.gca().transAxes)
      plt.text(0.24, 0.9, "Strip", fontstyle='italic', fontsize=22, transform=plt.gca().transAxes)
      plt.text(0.05, 0.83, "{} Batch Sample".format(self.__batchType), fontsize=22, transform=plt.gca().transAxes)
      if self.__batchType == 'Production':
        plt.text(0.05, 0.76, "Pre data from 80/80 GaNFETs", fontsize=22, transform=plt.gca().transAxes)
        plt.text(0.05, 0.69, "Post data from 76/80 GaNFETs", fontsize=22, transform=plt.gca().transAxes)
        plt.legend(loc=(0.03, 0.52), ncol=1, numpoints=5, fontsize=22).get_frame().set_linewidth(0.0)
      else:
        plt.text(0.05, 0.76, "Data from 16/16 GaNFETs", fontsize=22, transform=plt.gca().transAxes)
        plt.legend(loc=(0.03, 0.59), ncol=1, numpoints=5, fontsize=22).get_frame().set_linewidth(0.0)
      plt.yticks(fontsize=22)
      plt.xticks(fontsize=22)
    else:
      plt.xlabel(r'$\mathregular{V_{DS}\ range\ (x\,\pm\,10V)}$ [V]')
      plt.ylabel(r'$\mathregular{\frac{\Delta I_{D}}{\Delta V_{DS}}}\ \left[\frac{nA}{V}\right]$')
      plt.text(0.05, 0.90, "ATLAS ITk", fontstyle='italic', fontweight='bold', transform=plt.gca().transAxes)
      plt.text(0.205, 0.90, "Strip", fontstyle='italic', transform=plt.gca().transAxes)
      plt.text(0.05, 0.84, "{} Batch Sample".format(self.__batchType), transform=plt.gca().transAxes)
      if self.__batchType == 'Production':
        plt.text(0.05, 0.78, "Pre data from 80/80 GaNFETs", transform=plt.gca().transAxes)
        plt.text(0.05, 0.72, "Post data from 76/80 GaNFETs", transform=plt.gca().transAxes)
        plt.legend(loc=(0.03, 0.58), ncol=1, numpoints=5).get_frame().set_linewidth(0.0)
      else:
        plt.text(0.05, 0.78, "Data from 16/16 GaNFETs", transform=plt.gca().transAxes)
        plt.legend(loc=(0.03, 0.64), ncol=1, numpoints=5).get_frame().set_linewidth(0.0)

    # Save figure
    try:
      if log:
        plt.savefig('./../data/plots/'+fileName+"Log.png", format='png', dpi=200)
        plt.savefig('./../data/plots/'+fileName+"Log.pdf", format='pdf', dpi=200)
        plt.savefig('./../data/plots/'+fileName+"Log.eps", format='eps', dpi=200)
      else:
        plt.savefig('./../data/plots/'+fileName+".png", format='png', dpi=200)
        plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
        plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)
    except:
      os.system("mkdir ./../data/plots")
      if log:
        plt.savefig('./../data/plots/'+fileName+"Log.png", format='png', dpi=200)
        plt.savefig('./../data/plots/'+fileName+"Log.pdf", format='pdf', dpi=200)
        plt.savefig('./../data/plots/'+fileName+"Log.eps", format='eps', dpi=200)
      else:
        plt.savefig('./../data/plots/'+fileName+".png", format='png', dpi=200)
        plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
        plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)

    # Close plot
    plt.close()

  def plotIdAtVds550VFromOffState(self, largeFont=False):
    # Activate ATLAS style
    ampl.use_atlas_style()

    # General variables
    fileName = "merged{}OffStateIdAtVds550V".format(self.__batchType.replace("-",""))

    # Get data
    offState550Vpres = []
    offState550Vposts = []
    for fet in self.__ganfets:
      offState550Vpre,offState550Vpost,offState550Vscaling = fet.currLeakageIdAt550()
      if offState550Vpre != -99 or offState550Vpost != -99 or offState550Vscaling != -99:
        offState550Vpres.append(offState550Vpre*1e-3)
        offState550Vposts.append(offState550Vpost*1e-3)

    # Get distribution statistics
    preMean = np.mean(offState550Vpres)
    preStdev = statistics.stdev(offState550Vpres)
    postMean = np.mean(offState550Vposts)
    postStdev = statistics.stdev(offState550Vposts)

    # Get bin edges
    bins=np.histogram(np.hstack((offState550Vpres,offState550Vposts)), bins=20)[1]

    # Plot
    plt.hist(offState550Vpres, bins, color='black', alpha=0.5, label='Pre Gamma Irradiation')
    plt.hist(offState550Vposts, bins, color='red', alpha=0.5, label='Post Gamma Irradiation')

    # Plot editing
    plt.xlim([-20,500])
    xmin, xmax, ymin, ymax = plt.axis()
    if largeFont:
      plt.xlabel(r'$\mathregular{I_{D}}$ at $\mathregular{V_{DS}=550\,V}$ $[nA]$', fontsize=22)
      plt.ylabel(r'Number of GaNFETs', fontsize=22)
      plt.text((xmax-xmin)*0.45+xmin, (ymax-ymin)*0.9+ymin, "ATLAS ITk", fontstyle='italic', fontweight='bold', fontsize=22)
      plt.text((xmax-xmin)*0.64+xmin, (ymax-ymin)*0.9+ymin, "Strip", fontstyle='italic', fontsize=22)
      plt.text((xmax-xmin)*0.45+xmin, (ymax-ymin)*0.83+ymin, "{} Batch Sample".format(self.__batchType), fontsize=22)
      if self.__batchType == 'Production':
        plt.text((xmax-xmin)*0.45+xmin, (ymax-ymin)*0.76+ymin, "Data from 76/80 GaNFETs", fontsize=22)
      else:
        plt.text((xmax-xmin)*0.45+xmin, (ymax-ymin)*0.76+ymin, "Data from 16/16 GaNFETs", fontsize=22)
      plt.legend(loc=(0.44, 0.59), ncol=1, numpoints=5, fontsize=22).get_frame().set_linewidth(0.0)
      plt.text((xmax-xmin)*0.45+xmin, (ymax-ymin)*0.545+ymin, r"$\mathregular{(\overline{x}\,\pm\,\sigma)_{pre}}$"+r" = ({:.2f}".format(preMean)+r"$\,\mathregular{\pm}\,$"+r"{:.2f})$\,$nA".format(preStdev), fontsize=22)
      plt.text((xmax-xmin)*0.45+xmin, (ymax-ymin)*0.475+ymin, r"$\mathregular{(\overline{x}\,\pm\,\sigma)_{post}}$"+r" = ({:.2f}".format(postMean)+r"$\,\mathregular{\pm}\,$"+r"{:.2f})$\,$nA".format(postStdev), fontsize=22)
      plt.yticks(fontsize=22)
      plt.xticks(fontsize=22)
    else:
      plt.xlabel(r'$\mathregular{I_{D}}$ at $\mathregular{V_{DS}=550\,V}$ $[nA]$')
      plt.ylabel(r'Number of GaNFETs')
      plt.text((xmax-xmin)*0.5+xmin, (ymax-ymin)*0.90+ymin, "ATLAS ITk", fontstyle='italic', fontweight='bold')
      plt.text((xmax-xmin)*0.655+xmin, (ymax-ymin)*0.9+ymin, "Strip", fontstyle='italic')
      plt.text((xmax-xmin)*0.5+xmin, (ymax-ymin)*0.84+ymin, "{} Batch Sample".format(self.__batchType))
      if self.__batchType == 'Production':
        plt.text((xmax-xmin)*0.5+xmin, (ymax-ymin)*0.78+ymin, "Data from 76/80 GaNFETs")
      else:
        plt.text((xmax-xmin)*0.5+xmin, (ymax-ymin)*0.78+ymin, "Data from 16/16 GaNFETs")
      plt.legend(loc=(0.49, 0.64), ncol=1, numpoints=5).get_frame().set_linewidth(0.0)
      plt.text((xmax-xmin)*0.5+xmin, (ymax-ymin)*0.605+ymin, r"$\mathregular{(\overline{x}\,\pm\,\sigma)_{pre}}$"+r" = ({:.2f}".format(preMean)+r"$\,\mathregular{\pm}\,$"+r"{:.2f})$\,$nA".format(preStdev))
      plt.text((xmax-xmin)*0.5+xmin, (ymax-ymin)*0.545+ymin, r"$\mathregular{(\overline{x}\,\pm\,\sigma)_{post}}$"+r" = ({:.2f}".format(postMean)+r"$\,\mathregular{\pm}\,$"+r"{:.2f})$\,$nA".format(postStdev))

    # Save figure
    try:
      plt.savefig('./../data/plots/'+fileName+".png", format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)
    except:
      os.system("mkdir ./../data/plots")
      plt.savefig('./../data/plots/'+fileName+".png", format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)

    # Close plot
    plt.close()

  def plotIdChangeAtVds550VFromOffState(self, largeFont=False):
    # Activate ATLAS style
    ampl.use_atlas_style()

    # General variables
    fileName = "merged{}OffStateIdChangeAtVds550V".format(self.__batchType.replace("-",""))

    # Get relevant ganfet variables
    offState550Vscalings = []
    for fet in self.__ganfets:
      offState550Vpre,offState550Vpost,offState550Vscaling = fet.currLeakageIdAt550()
      if offState550Vpre != -99 or offState550Vpost != -99 or offState550Vscaling != -99:
        offState550Vscalings.append(offState550Vscaling*100)

    # Get distribution statistics
    mean = statistics.mean(offState550Vscalings)
    stdev = statistics.stdev(offState550Vscalings)

    # Plot
    plt.hist(offState550Vscalings, bins=20, color='blue', alpha=0.5)

    # Plot editing
    plt.xlim([-130,450])
    xmin, xmax, ymin, ymax = plt.axis()
    if largeFont:
      plt.xlabel(r'$\mathregular{I_{D}}$ change at $\mathregular{V_{DS}=550\,V}$ [%]', fontsize=22)
      plt.ylabel(r'Number of GaNFETs', fontsize=22)
      plt.text((xmax-xmin)*0.515+xmin, (ymax-ymin)*0.9+ymin, "ATLAS ITk", fontstyle='italic', fontweight='bold', fontsize=22)
      plt.text((xmax-xmin)*0.705+xmin, (ymax-ymin)*0.9+ymin, "Strip", fontstyle='italic', fontsize=22)
      plt.text((xmax-xmin)*0.515+xmin, (ymax-ymin)*0.83+ymin, "{} Batch Sample".format(self.__batchType), fontsize=22)
      if self.__batchType == 'Production':
        plt.text((xmax-xmin)*0.515+xmin, (ymax-ymin)*0.76+ymin, "Data from 76/80 GaNFETs", fontsize=22)
      else:
        plt.text((xmax-xmin)*0.515+xmin, (ymax-ymin)*0.76+ymin, "Data from 16/16 GaNFETs", fontsize=22)
      plt.text((xmax-xmin)*0.515+xmin, (ymax-ymin)*0.69+ymin, r"$\mathregular{\overline{x}\,\pm\,\sigma}$"+r" = ({:.2f}".format(mean)+r"$\,\mathregular{\pm}\,$"+r"{:.2f})%".format(stdev), fontsize=22)
      plt.yticks(fontsize=22)
      plt.xticks(fontsize=22)
    else:
      plt.xlabel(r'$\mathregular{I_{D}}$ change at $\mathregular{V_{DS}=550\,V}$ [%]')
      plt.ylabel(r'Number of GaNFETs')
      plt.text((xmax-xmin)*0.565+xmin, (ymax-ymin)*0.90+ymin, "ATLAS ITk", fontstyle='italic', fontweight='bold')
      plt.text((xmax-xmin)*0.715+xmin, (ymax-ymin)*0.9+ymin, "Strip", fontstyle='italic')
      plt.text((xmax-xmin)*0.565+xmin, (ymax-ymin)*0.84+ymin, "{} Batch Sample".format(self.__batchType))
      if self.__batchType == 'Production':
        plt.text((xmax-xmin)*0.565+xmin, (ymax-ymin)*0.78+ymin, "Data from 76/80 GaNFETs")
      else:
        plt.text((xmax-xmin)*0.565+xmin, (ymax-ymin)*0.78+ymin, "Data from 80/80 GaNFETs")
      plt.text((xmax-xmin)*0.565+xmin, (ymax-ymin)*0.72+ymin, r"$\mathregular{\overline{x}\,\pm\,\sigma}$"+r" = ({:.2f}".format(mean)+r"$\,\mathregular{\pm}\,$"+r"{:.2f})%".format(stdev))

    # Save figure
    try:
      plt.savefig('./../data/plots/'+fileName+".png", format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)
    except:
      os.system("mkdir ./../data/plots")
      plt.savefig('./../data/plots/'+fileName+".png", format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)

    # Close plot
    plt.close()
  
  def plotIdAtVds250VFromOffState(self, largeFont=False):
    # Activate ATLAS style
    ampl.use_atlas_style()

    # General variables
    fileName = "merged{}OffStateIdAtVds250V".format(self.__batchType.replace("-",""))

    # Get data
    offState250Vpres = []
    offState250Vposts = []
    for fet in self.__ganfets:
      offState250Vpre,offState250Vpost,offState250Vscaling = fet.currLeakageIdAt250()
      if offState250Vpre != -99 or offState250Vpost != -99 or offState250Vscaling != -99:
        offState250Vpres.append(offState250Vpre*1e-3)
        offState250Vposts.append(offState250Vpost*1e-3)

    # Get distribution statistics
    preMean = np.mean(offState250Vpres)
    preStdev = statistics.stdev(offState250Vpres)
    postMean = np.mean(offState250Vposts)
    postStdev = statistics.stdev(offState250Vposts)

    # Get bin edges
    bins=np.histogram(np.hstack((offState250Vpres,offState250Vposts)), bins=20)[1]

    # Plot
    plt.hist(offState250Vpres, bins, color='black', alpha=0.5, label='Pre Gamma Irradiation')
    plt.hist(offState250Vposts, bins, color='red', alpha=0.5, label='Post Gamma Irradiation')

    # Plot editing
    plt.xlim([-8,80])
    xmin, xmax, ymin, ymax = plt.axis()
    if largeFont:
      plt.xlabel(r'$\mathregular{I_{D}}$ at $\mathregular{V_{DS}=250\,V}$ $[nA]$', fontsize=22)
      plt.ylabel(r'Number of GaNFETs', fontsize=22)
      plt.text((xmax-xmin)*0.45+xmin, (ymax-ymin)*0.9+ymin, "ATLAS ITk", fontstyle='italic', fontweight='bold', fontsize=22)
      plt.text((xmax-xmin)*0.64+xmin, (ymax-ymin)*0.9+ymin, "Strip", fontstyle='italic', fontsize=22)
      plt.text((xmax-xmin)*0.45+xmin, (ymax-ymin)*0.83+ymin, "{} Batch Sample".format(self.__batchType), fontsize=22)
      if self.__batchType == 'Production':
        plt.text((xmax-xmin)*0.45+xmin, (ymax-ymin)*0.76+ymin, "Data from 76/80 GaNFETs", fontsize=22)
      else:
        plt.text((xmax-xmin)*0.45+xmin, (ymax-ymin)*0.76+ymin, "Data from 16/16 GaNFETs", fontsize=22)
      plt.legend(loc=(0.44, 0.59), ncol=1, numpoints=5, fontsize=22).get_frame().set_linewidth(0.0)
      plt.text((xmax-xmin)*0.45+xmin, (ymax-ymin)*0.545+ymin, r"$\mathregular{(\overline{x}\,\pm\,\sigma)_{pre}}$"+r" = ({:.2f}".format(preMean)+r"$\,\mathregular{\pm}\,$"+r"{:.2f})$\,$nA".format(preStdev), fontsize=22)
      plt.text((xmax-xmin)*0.45+xmin, (ymax-ymin)*0.475+ymin, r"$\mathregular{(\overline{x}\,\pm\,\sigma)_{post}}$"+r" = ({:.2f}".format(postMean)+r"$\,\mathregular{\pm}\,$"+r"{:.2f})$\,$nA".format(postStdev), fontsize=22)
      plt.yticks(fontsize=22)
      plt.xticks(fontsize=22)
    else:
      plt.xlabel(r'$\mathregular{I_{D}}$ at $\mathregular{V_{DS}=250\,V}$ $[nA]$')
      plt.ylabel(r'Number of GaNFETs')
      plt.text((xmax-xmin)*0.5+xmin, (ymax-ymin)*0.90+ymin, "ATLAS ITk", fontstyle='italic', fontweight='bold')
      plt.text((xmax-xmin)*0.655+xmin, (ymax-ymin)*0.9+ymin, "Strip", fontstyle='italic')
      plt.text((xmax-xmin)*0.5+xmin, (ymax-ymin)*0.84+ymin, "{} Batch Sample".format(self.__batchType))
      if self.__batchType == 'Production':
        plt.text((xmax-xmin)*0.5+xmin, (ymax-ymin)*0.78+ymin, "Data from 76/80 GaNFETs")
      else:
        plt.text((xmax-xmin)*0.5+xmin, (ymax-ymin)*0.78+ymin, "Data from 16/16 GaNFETs")
      plt.legend(loc=(0.49, 0.64), ncol=1, numpoints=5).get_frame().set_linewidth(0.0)
      plt.text((xmax-xmin)*0.5+xmin, (ymax-ymin)*0.605+ymin, r"$\mathregular{(\overline{x}\,\pm\,\sigma)_{pre}}$"+r" = ({:.2f}".format(preMean)+r"$\,\mathregular{\pm}\,$"+r"{:.2f})$\,$nA".format(preStdev))
      plt.text((xmax-xmin)*0.5+xmin, (ymax-ymin)*0.545+ymin, r"$\mathregular{(\overline{x}\,\pm\,\sigma)_{post}}$"+r" = ({:.2f}".format(postMean)+r"$\,\mathregular{\pm}\,$"+r"{:.2f})$\,$nA".format(postStdev))

    # Save figure
    try:
      plt.savefig('./../data/plots/'+fileName+".png", format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)
    except:
      os.system("mkdir ./../data/plots")
      plt.savefig('./../data/plots/'+fileName+".png", format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)

    # Close plot
    plt.close()

  def plotIdChangeAtVds250VFromOffState(self, largeFont=False):
    # Activate ATLAS style
    ampl.use_atlas_style()

    # General variables
    fileName = "merged{}OffStateIdChangeAtVds250V".format(self.__batchType.replace("-",""))

    # Get relevant ganfet variables
    offState250Vscalings = []
    for fet in self.__ganfets:
      offState250Vpre,offState250Vpost,offState250Vscaling = fet.currLeakageIdAt250()
      if offState250Vpre != -99 or offState250Vpost != -99 or offState250Vscaling != -99:
        offState250Vscalings.append(offState250Vscaling*100)

    # Get distribution statistics
    mean = statistics.mean(offState250Vscalings)
    stdev = statistics.stdev(offState250Vscalings)

    # Plot
    plt.hist(offState250Vscalings, bins=20, color='blue', alpha=0.5)

    # Plot editing
    plt.xlim([-130,1500])
    xmin, xmax, ymin, ymax = plt.axis()
    if largeFont:
      plt.xlabel(r'$\mathregular{I_{D}}$ change at $\mathregular{V_{DS}=250\,V}$ [%]', fontsize=22)
      plt.ylabel(r'Number of GaNFETs', fontsize=22)
      plt.text((xmax-xmin)*0.515+xmin, (ymax-ymin)*0.9+ymin, "ATLAS ITk", fontstyle='italic', fontweight='bold', fontsize=22)
      plt.text((xmax-xmin)*0.705+xmin, (ymax-ymin)*0.9+ymin, "Strip", fontstyle='italic', fontsize=22)
      plt.text((xmax-xmin)*0.515+xmin, (ymax-ymin)*0.83+ymin, "{} Batch Sample".format(self.__batchType), fontsize=22)
      if self.__batchType == 'Production':
        plt.text((xmax-xmin)*0.515+xmin, (ymax-ymin)*0.76+ymin, "Data from 76/80 GaNFETs", fontsize=22)
      else:
        plt.text((xmax-xmin)*0.515+xmin, (ymax-ymin)*0.76+ymin, "Data from 16/16 GaNFETs", fontsize=22)
      plt.text((xmax-xmin)*0.515+xmin, (ymax-ymin)*0.69+ymin, r"$\mathregular{\overline{x}\,\pm\,\sigma}$"+r" = ({:.2f}".format(mean)+r"$\,\mathregular{\pm}\,$"+r"{:.2f})%".format(stdev), fontsize=22)
      plt.yticks(fontsize=22)
      plt.xticks(fontsize=22)
    else:
      plt.xlabel(r'$\mathregular{I_{D}}$ change at $\mathregular{V_{DS}=250\,V}$ [%]')
      plt.ylabel(r'Number of GaNFETs')
      plt.text((xmax-xmin)*0.565+xmin, (ymax-ymin)*0.90+ymin, "ATLAS ITk", fontstyle='italic', fontweight='bold')
      plt.text((xmax-xmin)*0.715+xmin, (ymax-ymin)*0.9+ymin, "Strip", fontstyle='italic')
      plt.text((xmax-xmin)*0.565+xmin, (ymax-ymin)*0.84+ymin, "{} Batch Sample".format(self.__batchType))
      if self.__batchType == 'Production':
        plt.text((xmax-xmin)*0.565+xmin, (ymax-ymin)*0.78+ymin, "Data from 76/80 GaNFETs")
      else:
        plt.text((xmax-xmin)*0.565+xmin, (ymax-ymin)*0.78+ymin, "Data from 16/16 GaNFETs")
      plt.text((xmax-xmin)*0.565+xmin, (ymax-ymin)*0.72+ymin, r"$\mathregular{\overline{x}\,\pm\,\sigma}$"+r" = ({:.2f}".format(mean)+r"$\,\mathregular{\pm}\,$"+r"{:.2f})%".format(stdev))

    # Save figure
    try:
      plt.savefig('./../data/plots/'+fileName+".png", format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)
    except:
      os.system("mkdir ./../data/plots")
      plt.savefig('./../data/plots/'+fileName+".png", format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)

    # Close plot
    plt.close()

  def plotIgFromOffState(self, largeFont=False):
    # Activate ATLAS style
    ampl.use_atlas_style()

    # General variables
    fileName = 'merged{}OffStateIG'.format(self.__batchType.replace("-",""))

    # Get and plot data
    for fet in self.__ganfets:
      prefix = 'ganfet '+str(fet.num())+' '
      
      # Get pre and post irradiation data
      xPre = fet.offStatePreIrrad()[prefix+'VGS'].to_numpy()
      yPre = fet.offStatePreIrrad()[prefix+'IG'].to_numpy()*1e-3
      xPost = fet.offStatePostIrrad()[prefix+'VGS'].to_numpy()
      yPost = fet.offStatePostIrrad()[prefix+'IG'].to_numpy()*1e-3
      
      # Plot pre irradiation data
      if fet.num() == 1:
        plt.plot(xPre, yPre, color='black', marker='.', markersize=3, alpha = 0.3, label="Pre Gamma Irradiation")
        plt.plot(xPost, yPost, color='red', marker='.', markersize=3, alpha = 0.3, label="Post Gamma Irradiation")
      else:
        plt.plot(xPre, yPre, color='black', marker='.', markersize=3, alpha = 0.3)
        plt.plot(xPost, yPost, color='red', marker='.', markersize=3, alpha = 0.3)

    # Edit plotting
    plt.ticklabel_format(axis='y', style='sci', scilimits=(0,0))
    plt.xlim([0,600])
    plt.ylim([-20,500])
    xmin, xmax, ymin, ymax = plt.axis()
    if largeFont:
      plt.xlabel(r'$\mathregular{V_{DS}}$ [V]', fontsize=22)
      plt.ylabel(r'$\mathregular{I_{G}}$ [nA]', fontsize=22)
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.9+ymin, "ATLAS ITk", fontstyle='italic', fontweight='bold', fontsize=22)
      plt.text((xmax-xmin)*0.24+xmin, (ymax-ymin)*0.9+ymin, "Strip", fontstyle='italic', fontsize=22)
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.83+ymin, "{} Batch Sample".format(self.__batchType), fontsize=22)
      if self.__batchType == 'Production':
        plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.76+ymin, "Pre data from 80/80 GaNFETs", fontsize=22)
        plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.69+ymin, "Post data from 76/80 GaNFETs", fontsize=22)
        plt.legend(loc=(0.03, 0.52), ncol=1, numpoints=5, fontsize=22).get_frame().set_linewidth(0.0)
      else:
        plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.76+ymin, "Data from 16/16 GaNFETs", fontsize=22)
        plt.legend(loc=(0.03, 0.59), ncol=1, numpoints=5, fontsize=22).get_frame().set_linewidth(0.0)
      plt.yticks(fontsize=22)
      plt.xticks(fontsize=22)
    else:
      plt.xlabel(r'$\mathregular{V_{DS}}$ [V]')
      plt.ylabel(r'$\mathregular{I_{G}}$ [nA]')
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.90+ymin, "ATLAS ITk", fontstyle='italic', fontweight='bold')
      plt.text((xmax-xmin)*0.205+xmin, (ymax-ymin)*0.9+ymin, "Strip", fontstyle='italic')
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.84+ymin, "{} Batch Sample".format(self.__batchType))
      if self.__batchType == 'Production':
        plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.78+ymin, "Pre data from 80/80 GaNFETs")
        plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.72+ymin, "Post data from 76/80 GaNFETs")
        plt.legend(loc=(0.04, 0.58), ncol=1, numpoints=5).get_frame().set_linewidth(0.0)
      else:
        plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.78+ymin, "Data from 16/16 GaNFETs")
        plt.legend(loc=(0.04, 0.64), ncol=1, numpoints=5).get_frame().set_linewidth(0.0)

    # Save figure
    try:
      plt.savefig('./../data/plots/'+fileName+".png", format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)
    except:
      os.system("mkdir ./../data/plots")
      plt.savefig('./../data/plots/'+fileName+".png", format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)

    # Close plot
    plt.close()

  def plotIsFromOffState(self, largeFont=False):
    # Activate ATLAS style
    ampl.use_atlas_style()

    # General variables
    fileName = 'merged{}OffStateIS'.format(self.__batchType.replace("-",""))

    # Get and plot data
    for fet in self.__ganfets:
      prefix = 'ganfet '+str(fet.num())+' '

      # Get pre and post irradiation data
      xPre = fet.offStatePreIrrad()[prefix+'VGS'].to_numpy()
      yPre = fet.offStatePreIrrad()[prefix+'IS'].to_numpy()*1e-3
      xPost = fet.offStatePostIrrad()[prefix+'VGS'].to_numpy()
      yPost = fet.offStatePostIrrad()[prefix+'IS'].to_numpy()*1e-3

      # Plot pre irradiation data
      if fet.num() == 1:
        plt.plot(xPre, yPre, color='black', marker='.', markersize=3, alpha = 0.3, label="Pre Gamma Irradiation")
        plt.plot(xPost, yPost, color='red', marker='.', markersize=3, alpha = 0.3, label="Post Gamma Irradiation")
      else:
        plt.plot(xPre, yPre, color='black', marker='.', markersize=3, alpha = 0.3)
        plt.plot(xPost, yPost, color='red', marker='.', markersize=3, alpha = 0.3)

    # Edit plotting
    plt.ticklabel_format(axis='y', style='sci', scilimits=(0,0))
    plt.xlim([0,600])
    plt.ylim([-10,160])
    xmin, xmax, ymin, ymax = plt.axis()
    if largeFont:
      plt.xlabel(r'$\mathregular{V_{DS}}$ [V]', fontsize=22)
      plt.ylabel(r'$\mathregular{I_{S}}$ [nA]', fontsize=22)
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.9+ymin, "ATLAS ITk", fontstyle='italic', fontweight='bold', fontsize=22)
      plt.text((xmax-xmin)*0.24+xmin, (ymax-ymin)*0.9+ymin, "Strip", fontstyle='italic', fontsize=22)
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.83+ymin, "{} Batch Sample".format(self.__batchType), fontsize=22)
      if self.__batchType == 'Production':
        plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.76+ymin, "Pre data from 80/80 GaNFETs", fontsize=22)
        plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.69+ymin, "Post data from 76/80 GaNFETs", fontsize=22)
        plt.legend(loc=(0.03, 0.52), ncol=1, numpoints=5, fontsize=22).get_frame().set_linewidth(0.0)
      else:
        plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.76+ymin, "Data from 16/16 GaNFETs", fontsize=22)
        plt.legend(loc=(0.03, 0.59), ncol=1, numpoints=5, fontsize=22).get_frame().set_linewidth(0.0)
      plt.yticks(fontsize=22)
      plt.xticks(fontsize=22)
    else:
      plt.xlabel(r'$\mathregular{V_{DS}}$ [V]')
      plt.ylabel(r'$\mathregular{I_{S}}$ [nA]')
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.90+ymin, "ATLAS ITk", fontstyle='italic', fontweight='bold')
      plt.text((xmax-xmin)*0.205+xmin, (ymax-ymin)*0.9+ymin, "Strip", fontstyle='italic')
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.84+ymin, "{} Batch Sample".format(self.__batchType))
      if self.__batchType == 'Production':
        plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.78+ymin, "Pre data from 80/80 GaNFETs")
        plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.72+ymin, "Post data from 76/80 GaNFETs")
        plt.legend(loc=(0.04, 0.58), ncol=1, numpoints=5).get_frame().set_linewidth(0.0)
      else:
        plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.78+ymin, "Data from 16/16 GaNFETs")
        plt.legend(loc=(0.04, 0.64), ncol=1, numpoints=5).get_frame().set_linewidth(0.0)

    # Save figure
    try:
      plt.savefig('./../data/plots/'+fileName+".png", format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)
    except:
      os.system("mkdir ./../data/plots")
      plt.savefig('./../data/plots/'+fileName+".png", format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)

    # Close plot
    plt.close()

  def plotOnState1Gig(self, largeFont=False):
    # Activate ATLAS style
    ampl.use_atlas_style()

    # General variables
    fileName = 'merged{}OnState1Gig'.format(self.__batchType.replace("-",""))

    # Get and plot data
    for fet in self.__ganfets:
      prefix = 'ganfet '+str(fet.num())+' '

      # Get pre and post irradiation data
      xPre = fet.onState1GigPreIrrad()[prefix+'VGS'].to_numpy()
      yPre = fet.onState1GigPreIrrad()[prefix+'ID=IS+IG'].to_numpy()*1e-3
      xPost = fet.onState1GigPostIrrad()[prefix+'VGS'].to_numpy()
      yPost = fet.onState1GigPostIrrad()[prefix+'ID=IS+IG'].to_numpy()*1e-3

      # Plot pre irradiation data
      if fet.num() == 1:
        plt.plot(xPre, yPre, color='black', marker='.', markersize=3, alpha = 0.3, label="Pre Gamma Irradiation")
        plt.plot(xPost, yPost, color='red', marker='.', markersize=3, alpha = 0.3, label="Post Gamma Irradiation")
      else:
        plt.plot(xPre, yPre, color='black', marker='.', markersize=3, alpha = 0.3)
        plt.plot(xPost, yPost, color='red', marker='.', markersize=3, alpha = 0.3)

    # Edit plotting
    plt.ticklabel_format(axis='y', style='sci', scilimits=(0,0))
    plt.xlim([0,2.5])
    if self.__batchType == 'Production':
      plt.ylim([-40,1050])
    else:
      plt.ylim([-40,950])
    xmin, xmax, ymin, ymax = plt.axis()
    if largeFont:
      plt.xlabel(r'$\mathregular{V_{GS}}$ [V]', fontsize=22)
      plt.ylabel(r'$\mathregular{I_{D}}$ using 1$\,G\mathregular{\Omega}\ [nA]$', fontsize=22)
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.9+ymin, "ATLAS ITk", fontstyle='italic', fontweight='bold', fontsize=22)
      plt.text((xmax-xmin)*0.24+xmin, (ymax-ymin)*0.9+ymin, "Strip", fontstyle='italic', fontsize=22)
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.83+ymin, "{} Batch Sample".format(self.__batchType), fontsize=22)
      if self.__batchType == 'Production':
        plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.76+ymin, "Pre data from 80/80 GaNFETs", fontsize=22)
        plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.69+ymin, "Post data from 79/80 GaNFETs", fontsize=22)
        plt.legend(loc=(0.03, 0.52), ncol=1, numpoints=5, fontsize=22).get_frame().set_linewidth(0.0)
      else:
        plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.76+ymin, "Data from 80/80 GaNFETs", fontsize=22)
        plt.legend(loc=(0.03, 0.59), ncol=1, numpoints=5, fontsize=22).get_frame().set_linewidth(0.0)
      plt.yticks(fontsize=22)
      plt.xticks(fontsize=22)
    else:
      plt.xlabel(r'$\mathregular{V_{GS}}$ [V]')
      plt.ylabel(r'$\mathregular{I_{D}}$ using 1$\,G\mathregular{\Omega}\ [nA]$')
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.90+ymin, "ATLAS ITk", fontstyle='italic', fontweight='bold')
      plt.text((xmax-xmin)*0.205+xmin, (ymax-ymin)*0.9+ymin, "Strip", fontstyle='italic')
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.84+ymin, "{} Batch Sample".format(self.__batchType))
      if self.__batchType == 'Production':
        plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.78+ymin, "Pre data from 80/80 GaNFETs")
        plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.72+ymin, "Post data from 79/80 GaNFETs")
        plt.legend(loc=(0.04, 0.58), ncol=1, numpoints=5).get_frame().set_linewidth(0.0)
      else:
        plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.78+ymin, "Data from 80/80 GaNFETs")
        plt.legend(loc=(0.04, 0.64), ncol=1, numpoints=5).get_frame().set_linewidth(0.0)

    # Save figure
    try:
      plt.savefig('./../data/plots/'+fileName+".png", format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)
    except:
      os.system("mkdir ./../data/plots")
      plt.savefig('./../data/plots/'+fileName+".png", format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)

    # Close plot
    plt.close()

  def plotIdAtVgs0VFromOnState1Gig(self, largeFont=False):
    # Activate ATLAS style
    ampl.use_atlas_style()

    # General variables
    fileName = "merged{}OnState1GigIdAtVgs0V".format(self.__batchType.replace("-",""))

    # Get relevant ganfet variables
    onState1Gig0Vpres = []
    onState1Gig0Vposts = []
    for fet in self.__ganfets:
      onState1Gig0Vpre,onState1Gig0Vpost,onState1Gig0Vscaling = fet.curr1GId()
      if (onState1Gig0Vpre != -99 or onState1Gig0Vpost != -99 or onState1Gig0Vscaling != -99) and (self.__batchType == 'Pre-production' or (self.__batchType == 'Production' and fet.num() not in [9, 20, 60, 70])):
        onState1Gig0Vpres.append(onState1Gig0Vpre*1e-3)
        onState1Gig0Vposts.append(onState1Gig0Vpost*1e-3)

    # Get distribution statistics
    preMean = statistics.mean(onState1Gig0Vpres)
    preStdev = statistics.stdev(onState1Gig0Vpres)
    postMean = statistics.mean(onState1Gig0Vposts)
    postStdev = statistics.stdev(onState1Gig0Vposts)

    # Plot
    if self.__batchType == 'Production':
      plt.hist(onState1Gig0Vpres, bins=50, range=(10, 225), color='black', alpha=0.5, label='Pre Gamma Irradiation')
      plt.hist(onState1Gig0Vposts, bins=50, range=(10, 225), color='red', alpha=0.5, label='Post Gamma Irradiation')
    else:
      plt.hist(onState1Gig0Vpres, bins=50, range=(1, 120), color='black', alpha=0.5, label='Pre Gamma Irradiation')
      plt.hist(onState1Gig0Vposts, bins=50, range=(1, 120), color='red', alpha=0.5, label='Post Gamma Irradiation')

    # Plot editing
    if self.__batchType == 'Production':
      plt.xlim([10,225])
    else:
      plt.xlim([1,120])
    xmin, xmax, ymin, ymax = plt.axis()
    if largeFont:
      plt.xlabel(r'$\mathregular{I_{D}}$ at $\mathregular{V_{GS}=0}\,V$ $[nA]$', fontsize=22)
      plt.ylabel(r'Number of GaNFETs', fontsize=22)
      plt.text((xmax-xmin)*0.44+xmin, (ymax-ymin)*0.9+ymin, "ATLAS ITk", fontstyle='italic', fontweight='bold', fontsize=22)
      plt.text((xmax-xmin)*0.63+xmin, (ymax-ymin)*0.9+ymin, "Strip", fontstyle='italic', fontsize=22)
      plt.text((xmax-xmin)*0.44+xmin, (ymax-ymin)*0.83+ymin, "{} Batch Sample".format(self.__batchType), fontsize=22)
      if self.__batchType == 'Production':
        plt.text((xmax-xmin)*0.44+xmin, (ymax-ymin)*0.76+ymin, "Data from 76/80 GaNFETs", fontsize=22)
      else:
        plt.text((xmax-xmin)*0.44+xmin, (ymax-ymin)*0.76+ymin, "Data from 80/80 GaNFETs", fontsize=22)
      plt.legend(loc=(0.43, 0.59), ncol=1, numpoints=5, fontsize=22).get_frame().set_linewidth(0.0)
      plt.text((xmax-xmin)*0.44+xmin, (ymax-ymin)*0.545+ymin, r"$\mathregular{(\overline{x}\,\pm\,\sigma)_{pre}}$"+r" = ({:.2f}".format(preMean)+r"$\,\mathregular{\pm}\,$"+r"{:.2f})$\,$nA".format(preStdev), fontsize=22)
      plt.text((xmax-xmin)*0.44+xmin, (ymax-ymin)*0.475+ymin, r"$\mathregular{(\overline{x}\,\pm\,\sigma)_{post}}$"+r" = ({:.2f}".format(postMean)+r"$\,\mathregular{\pm}\,$"+r"{:.2f})$\,$nA".format(postStdev), fontsize=22)
      plt.yticks(fontsize=22)
      plt.xticks(fontsize=22)
    else:
      plt.xlabel(r'$\mathregular{I_{D}}$ at $\mathregular{V_{GS}=0}\,V$ $[nA]$')
      plt.ylabel(r'Number of GaNFETs')
      plt.text((xmax-xmin)*0.49+xmin, (ymax-ymin)*0.90+ymin, "ATLAS ITk", fontstyle='italic', fontweight='bold')
      plt.text((xmax-xmin)*0.645+xmin, (ymax-ymin)*0.9+ymin, "Strip", fontstyle='italic')
      plt.text((xmax-xmin)*0.49+xmin, (ymax-ymin)*0.84+ymin, "{} Batch Sample".format(self.__batchType))
      if self.__batchType == 'Production':
        plt.text((xmax-xmin)*0.49+xmin, (ymax-ymin)*0.78+ymin, "Data from 76/80 GaNFETs")
      else:
        plt.text((xmax-xmin)*0.49+xmin, (ymax-ymin)*0.78+ymin, "Data from 80/80 GaNFETs")
      plt.legend(loc=(0.48, 0.64), ncol=1, numpoints=5).get_frame().set_linewidth(0.0)
      plt.text((xmax-xmin)*0.49+xmin, (ymax-ymin)*0.605+ymin, r"$\mathregular{(\overline{x}\,\pm\,\sigma)_{pre}}$"+r" = ({:.2f}".format(preMean)+r"$\,\mathregular{\pm}\,$"+r"{:.2f})$\,$nA".format(preStdev))
      plt.text((xmax-xmin)*0.49+xmin, (ymax-ymin)*0.545+ymin, r"$\mathregular{(\overline{x}\,\pm\,\sigma)_{post}}$"+r" = ({:.2f}".format(postMean)+r"$\,\mathregular{\pm}\,$"+r"{:.2f})$\,$nA".format(postStdev))

    # Save figure
    try:
      plt.savefig('./../data/plots/'+fileName+".png", format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)
    except:
      os.system("mkdir ./../data/plots")
      plt.savefig('./../data/plots/'+fileName+".png", format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)

    # Close plot
    plt.close()

  def plotIdChangeAtVgs0VFromOnState1Gig(self, largeFont=False):
    # Activate ATLAS style
    ampl.use_atlas_style()

    # General variables
    fileName = "merged{}OnState1GigIdChangeAtVgs0V".format(self.__batchType.replace("-",""))

    # Get relevant ganfet variables
    onState1Gig0Vscalings = []
    for fet in self.__ganfets:
      onState1Gig0Vpre,onState1Gig0Vpost,onState1Gig0Vscaling = fet.curr1GId()
      if (onState1Gig0Vpre != -99 or onState1Gig0Vpost != -99 or onState1Gig0Vscaling != -99) and (self.__batchType == 'Pre-production' or (self.__batchType == 'Production' and fet.num() not in [9, 20, 60, 70])):
        onState1Gig0Vscalings.append(onState1Gig0Vscaling*100)

    # Get distribution statistics
    mean = statistics.mean(onState1Gig0Vscalings)
    stdev = statistics.stdev(onState1Gig0Vscalings)

    # Plot
    if self.__batchType == 'Production':
      plt.hist(onState1Gig0Vscalings, bins=25, range=(20,700), color='blue', alpha=0.5)
    else:
      plt.hist(onState1Gig0Vscalings, bins=25, range=(-150,650), color='blue', alpha=0.5)

    # Plot editing
    if self.__batchType == 'Production':
      plt.xlim([10, 710])
      xShift = 0.0
    else:
      plt.xlim([-150, 650])
      xShift = -0.05
    xmin, xmax, ymin, ymax = plt.axis()
    if largeFont:
      plt.xlabel(r'$\mathregular{I_{D}}$ change at $\mathregular{V_{GS}=0}\,V$ [%]', fontsize=22)
      plt.ylabel(r'Number of GaNFETs', fontsize=22)
      plt.text((xmax-xmin)*(0.52+xShift)+xmin, (ymax-ymin)*0.9+ymin, "ATLAS ITk", fontstyle='italic', fontweight='bold', fontsize=22)
      plt.text((xmax-xmin)*(0.71+xShift)+xmin, (ymax-ymin)*0.9+ymin, "Strip", fontstyle='italic', fontsize=22)
      plt.text((xmax-xmin)*(0.52+xShift)+xmin, (ymax-ymin)*0.83+ymin, "{} Batch Sample".format(self.__batchType), fontsize=22)
      if self.__batchType == 'Production':
        plt.text((xmax-xmin)*(0.52+xShift)+xmin, (ymax-ymin)*0.76+ymin, "Data from 76/80 GaNFETs", fontsize=22)
      else:
        plt.text((xmax-xmin)*(0.52+xShift)+xmin, (ymax-ymin)*0.76+ymin, "Data from 80/80 GaNFETs", fontsize=22)
      plt.text((xmax-xmin)*(0.52+xShift)+xmin, (ymax-ymin)*0.69+ymin, r"$\mathregular{\overline{x}\,\pm\,\sigma}$"+r" = ({:.2f}".format(mean)+r"$\,\mathregular{\pm}\,$"+r"{:.2f})%".format(stdev), fontsize=22)
      plt.yticks(fontsize=22)
      plt.xticks(fontsize=22)
    else:
      plt.xlabel(r'$\mathregular{I_{D}}$ change at $\mathregular{V_{GS}=0}\,V$ [%]')
      plt.ylabel(r'Number of GaNFETs')
      plt.text((xmax-xmin)*(0.58+xShift)+xmin, (ymax-ymin)*0.90+ymin, "ATLAS ITk Strip", fontstyle='italic', fontweight='bold')
      plt.text((xmax-xmin)*(0.735+xShift)+xmin, (ymax-ymin)*0.9+ymin, "Strip", fontstyle='italic', color='red')
      plt.text((xmax-xmin)*(0.58+xShift)+xmin, (ymax-ymin)*0.84+ymin, "{} Batch Sample".format(self.__batchType))
      if self.__batchType == 'Production':
        plt.text((xmax-xmin)*(0.58+xShift)+xmin, (ymax-ymin)*0.78+ymin, "Data from 76/80 GaNFETs")
      else:
        plt.text((xmax-xmin)*(0.58+xShift)+xmin, (ymax-ymin)*0.78+ymin, "Data from 80/80 GaNFETs")
      plt.text((xmax-xmin)*(0.58+xShift)+xmin, (ymax-ymin)*0.72+ymin, r"$\mathregular{\overline{x}\,\pm\,\sigma}$"+r" = ({:.2f}".format(mean)+r"$\,\mathregular{\pm}\,$"+r"{:.2f})%".format(stdev))

    # Save figure
    try:
      plt.savefig('./../data/plots/'+fileName+".png", format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)
    except:
      os.system("mkdir ./../data/plots")
      plt.savefig('./../data/plots/'+fileName+".png", format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)

    # Close plot
    plt.close()

  def plotIgFromOnState1Gig(self, largeFont=False):
    # Activate ATLAS style
    ampl.use_atlas_style()

    # General variables
    fileName = 'merged{}OnState1GigIG'.format(self.__batchType.replace("-",""))

    # Get and plot data
    for fet in self.__ganfets:
      prefix = 'ganfet '+str(fet.num())+' '

      # Get pre and post irradiation data
      xPre = fet.onState1GigPreIrrad()[prefix+'VGS'].to_numpy()
      yPre = fet.onState1GigPreIrrad()[prefix+'IG'].to_numpy()*1e-6
      xPost = fet.onState1GigPostIrrad()[prefix+'VGS'].to_numpy()
      yPost = fet.onState1GigPostIrrad()[prefix+'IG'].to_numpy()*1e-6

      # Plot pre irradiation data
      if fet.num() == 1:
        plt.plot(xPre, yPre, color='black', marker='.', markersize=3, alpha = 0.3, label="Pre Gamma Irradiation")
        plt.plot(xPost, yPost, color='red', marker='.', markersize=3, alpha = 0.3, label="Post Gamma Irradiation")
      else:
        plt.plot(xPre, yPre, color='black', marker='.', markersize=3, alpha = 0.3)
        plt.plot(xPost, yPost, color='red', marker='.', markersize=3, alpha = 0.3)

    # Edit plotting
    plt.ticklabel_format(axis='y', style='sci', scilimits=(0,0))
    plt.xlim([0,2.5])
    if self.__batchType == 'Production':
      plt.ylim([-110,110])
    else:
      plt.ylim([-110,90])
    xmin, xmax, ymin, ymax = plt.axis()
    if largeFont:
      plt.xlabel(r'$\mathregular{V_{GS}}$ [V]', fontsize=22)
      plt.ylabel(r'$\mathregular{I_{G}}$ using 1$\,$G$\mathregular{\Omega}$ [$\mu$A]', fontsize=22)
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.9+ymin, "ATLAS ITk", fontstyle='italic', fontweight='bold', fontsize=22)
      plt.text((xmax-xmin)*0.24+xmin, (ymax-ymin)*0.9+ymin, "Strip", fontstyle='italic', fontsize=22)
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.83+ymin, "{} Batch Sample".format(self.__batchType), fontsize=22)
      if self.__batchType == 'Production':
        plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.76+ymin, "Pre data from 80/80 GaNFETs", fontsize=22)
        plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.69+ymin, "Post data from 79/80 GaNFETs", fontsize=22)
        plt.legend(loc=(0.03, 0.52), ncol=1, numpoints=5, fontsize=22).get_frame().set_linewidth(0.0)
      else:
        plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.76+ymin, "Data from 80/80 GaNFETs", fontsize=22)
        plt.legend(loc=(0.03, 0.59), ncol=1, numpoints=5, fontsize=22).get_frame().set_linewidth(0.0)
      plt.yticks(fontsize=22)
      plt.xticks(fontsize=22)
    else:
      plt.xlabel(r'$\mathregular{V_{GS}}$ [V]')
      plt.ylabel(r'$\mathregular{I_{G}}$ using 1$\,$G$\mathregular{\Omega}$ [$\mu$A]')
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.90+ymin, "ATLAS ITk", fontstyle='italic', fontweight='bold')
      plt.text((xmax-xmin)*0.205+xmin, (ymax-ymin)*0.9+ymin, "Strip", fontstyle='italic')
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.84+ymin, "{} Batch Sample".format(self.__batchType))
      if self.__batchType == 'Production':
        plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.78+ymin, "Pre data from 80/80 GaNFETs")
        plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.72+ymin, "Post data from 79/80 GaNFETs")
        plt.legend(loc=(0.04, 0.58), ncol=1, numpoints=5).get_frame().set_linewidth(0.0)
      else:
        plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.78+ymin, "Data from 80/80 GaNFETs")
        plt.legend(loc=(0.04, 0.64), ncol=1, numpoints=5).get_frame().set_linewidth(0.0)

    # Save figure
    try:
      plt.savefig('./../data/plots/'+fileName+".png", format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)
    except:
      os.system("mkdir ./../data/plots")
      plt.savefig('./../data/plots/'+fileName+".png", format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)

    # Close plot
    plt.close()

  def plotIsFromOnState1Gig(self, largeFont=False):
    # Activate ATLAS style
    ampl.use_atlas_style()

    # General variables
    fileName = 'merged{}OnState1GigIS'.format(self.__batchType.replace("-",""))

    # Get and plot data
    for fet in self.__ganfets:
      prefix = 'ganfet '+str(fet.num())+' '
      
      # Get pre and post irradiation data
      xPre = fet.onState1GigPreIrrad()[prefix+'VGS'].to_numpy()
      yPre = fet.onState1GigPreIrrad()[prefix+'IS'].to_numpy()*1e-6
      xPost = fet.onState1GigPostIrrad()[prefix+'VGS'].to_numpy()
      yPost = fet.onState1GigPostIrrad()[prefix+'IS'].to_numpy()*1e-6

      # Plot pre irradiation data
      if fet.num() == 1:
        plt.plot(xPre, yPre, color='black', marker='.', markersize=3, alpha = 0.3, label="Pre Gamma Irradiation")
        plt.plot(xPost, yPost, color='red', marker='.', markersize=3, alpha = 0.3, label="Post Gamma Irradiation")
      else:
        plt.plot(xPre, yPre, color='black', marker='.', markersize=3, alpha = 0.3)
        plt.plot(xPost, yPost, color='red', marker='.', markersize=3, alpha = 0.3)

    # Edit plotting
    plt.ticklabel_format(axis='y', style='sci', scilimits=(0,0))
    plt.xlim([0,2.5])
    plt.ylim([-10,100])
    xmin, xmax, ymin, ymax = plt.axis()
    if largeFont:
      plt.xlabel(r'$\mathregular{V_{GS}}$ [V]', fontsize=22)
      plt.ylabel(r'$\mathregular{I_{S}}$ using 1$\,$G$\mathregular{\Omega}$ [$\mu$A]', fontsize=22)
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.9+ymin, "ATLAS ITk", fontstyle='italic', fontweight='bold', fontsize=22)
      plt.text((xmax-xmin)*0.24+xmin, (ymax-ymin)*0.9+ymin, "Strip", fontstyle='italic', fontsize=22)
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.83+ymin, "{} Batch Sample".format(self.__batchType), fontsize=22)
      if self.__batchType == 'Production':
        plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.76+ymin, "Pre data from 80/80 GaNFETs", fontsize=22)
        plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.69+ymin, "Post data from 79/80 GaNFETs", fontsize=22)
        plt.legend(loc=(0.03, 0.52), ncol=1, numpoints=5, fontsize=22).get_frame().set_linewidth(0.0)
      else:
        plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.76+ymin, "Data from 80/80 GaNFETs", fontsize=22)
        plt.legend(loc=(0.03, 0.59), ncol=1, numpoints=5, fontsize=22).get_frame().set_linewidth(0.0)
      plt.yticks(fontsize=22)
      plt.xticks(fontsize=22)
    else:
      plt.xlabel(r'$\mathregular{V_{GS}}$ [V]')
      plt.ylabel(r'$\mathregular{I_{S}}$ using 1$\,$G$\mathregular{\Omega}$ [$\mu$A]')
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.90+ymin, "ATLAS ITk", fontstyle='italic', fontweight='bold')
      plt.text((xmax-xmin)*0.205+xmin, (ymax-ymin)*0.9+ymin, "Strip", fontstyle='italic')
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.84+ymin, "{} Batch Sample".format(self.__batchType))
      if self.__batchType == 'Production':
        plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.78+ymin, "Pre data from 80/80 GaNFETs")
        plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.72+ymin, "Post data from 79/80 GaNFETs")
        plt.legend(loc=(0.04, 0.58), ncol=1, numpoints=5).get_frame().set_linewidth(0.0)
      else:
        plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.78+ymin, "Data from 80/80 GaNFETs")
        plt.legend(loc=(0.04, 0.64), ncol=1, numpoints=5).get_frame().set_linewidth(0.0)

    # Save figure
    try:
      plt.savefig('./../data/plots/'+fileName+".png", format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)
    except:
      os.system("mkdir ./../data/plots")
      plt.savefig('./../data/plots/'+fileName+".png", format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)

    # Close plot
    plt.close()

  def plotOnState265k(self, largeFont=False):
    # Activate ATLAS style
    ampl.use_atlas_style()

    # General variables
    fileName = 'merged{}OnState265k'.format(self.__batchType.replace("-",""))
    
    # Get and plot data
    for fet in self.__ganfets:
      prefix = 'ganfet '+str(fet.num())+' '

      # Get pre and post irradiation data
      xPre = fet.onState265kPreIrrad()[prefix+'VGS'].to_numpy()
      yPre = fet.onState265kPreIrrad()[prefix+'ID=IS+IG'].to_numpy()*1e-9
      xPost = fet.onState265kPostIrrad()[prefix+'VGS'].to_numpy()
      yPost = fet.onState265kPostIrrad()[prefix+'ID=IS+IG'].to_numpy()*1e-9

      # Plot pre irradiation data
      if fet.num() == 1:
        plt.plot(xPre, yPre, color='black', marker='.', markersize=3, alpha = 0.3, label="Pre Gamma Irradiation")
        plt.plot(xPost, yPost, color='red', marker='.', markersize=3, alpha = 0.3, label="Post Gamma Irradiation")
      else:
        plt.plot(xPre, yPre, color='black', marker='.', markersize=3, alpha = 0.3)
        plt.plot(xPost, yPost, color='red', marker='.', markersize=3, alpha = 0.3)

    # Edit plotting
    plt.ticklabel_format(axis='y', style='sci', scilimits=(0,0))
    plt.xlim([0,2.5])
    if self.__batchType == 'Production':
      plt.ylim([-0.2,4])
    else:
      plt.ylim([-0.2,3.5])
    xmin, xmax, ymin, ymax = plt.axis()
    if largeFont:
      plt.xlabel(r'$\mathregular{V_{GS}}$ [V]', fontsize=22)
      plt.ylabel(r'$\mathregular{I_{D}}$ using 265.4$\,$k$\mathregular{\Omega}$ [mA]', fontsize=22)
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.9+ymin, "ATLAS ITk", fontstyle='italic', fontweight='bold', fontsize=22)
      plt.text((xmax-xmin)*0.24+xmin, (ymax-ymin)*0.9+ymin, "Strip", fontstyle='italic', fontsize=22)
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.83+ymin, "{} Batch Sample".format(self.__batchType), fontsize=22)
      if self.__batchType == 'Production':
        plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.76+ymin, "Pre data from 80/80 GaNFETs", fontsize=22)
        plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.69+ymin, "Post data from 79/80 GaNFETs", fontsize=22)
        plt.legend(loc=(0.03, 0.52), ncol=1, numpoints=5, fontsize=22).get_frame().set_linewidth(0.0)
      else:
        plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.76+ymin, "Data from 80/80 GaNFETs", fontsize=22)
        plt.legend(loc=(0.03, 0.59), ncol=1, numpoints=5, fontsize=22).get_frame().set_linewidth(0.0)
      plt.yticks(fontsize=22)
      plt.xticks(fontsize=22)
    else:
      plt.xlabel(r'$\mathregular{V_{GS}}$ [V]')
      plt.ylabel(r'$\mathregular{I_{D}}$ using 265.4$\,$k$\mathregular{\Omega}$ [mA]')
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.90+ymin, "ATLAS ITk", fontstyle='italic', fontweight='bold')
      plt.text((xmax-xmin)*0.205+xmin, (ymax-ymin)*0.9+ymin, "Strip", fontstyle='italic')
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.84+ymin, "{} Batch Sample".format(self.__batchType))
      if self.__batchType == 'Production':
        plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.78+ymin, "Pre data from 80/80 GaNFETs")
        plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.72+ymin, "Post data from 79/80 GaNFETs")
        plt.legend(loc=(0.04, 0.58), ncol=1, numpoints=5).get_frame().set_linewidth(0.0)
      else:
        plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.78+ymin, "Data from 80/80 GaNFETs")
        plt.legend(loc=(0.04, 0.64), ncol=1, numpoints=5).get_frame().set_linewidth(0.0)

    # Save figure
    try:
      plt.savefig('./../data/plots/'+fileName+".png", format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)
    except:
      os.system("mkdir ./../data/plots")
      plt.savefig('./../data/plots/'+fileName+".png", format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)

    # Close plot
    plt.close()

  def plotIdAtVgs2p5VFromOnState265k(self, largeFont=False):
    # Activate ATLAS style
    ampl.use_atlas_style()

    # General variables
    fileName = "merged{}OnState265kIdAtVgs2p5V".format(self.__batchType.replace("-",""))

    # Get relevant ganfet variables
    onState265k2p5Vpres = []
    onState265k2p5Vposts = []
    for fet in self.__ganfets:
      onState265k2p5Vpre,onState265k2p5Vpost,onState265k2p5Vscaling = fet.curr265kId()
      if (onState265k2p5Vpre != -99 or onState265k2p5Vpost != -99 or onState265k2p5Vscaling != -99) and (self.__batchType == 'Pre-production' or (self.__batchType == 'Production' and fet.num() not in [9, 20, 60, 70])):
        onState265k2p5Vpres.append(onState265k2p5Vpre*1e-9)
        onState265k2p5Vposts.append(onState265k2p5Vpost*1e-9)

    # Get distribution statistics
    preMean = statistics.mean(onState265k2p5Vpres)
    preStdev = statistics.stdev(onState265k2p5Vpres)
    postMean = statistics.mean(onState265k2p5Vposts)
    postStdev = statistics.stdev(onState265k2p5Vposts)

    # Plot
    if self.__batchType == 'Production':
      plt.hist(onState265k2p5Vpres, bins=50, range=(1.785, 1.88), color='black', alpha=0.5, label='Pre Gamma Irradiation')
      plt.hist(onState265k2p5Vposts, bins=50, range=(1.785, 1.88), color='red', alpha=0.5, label='Post Gamma Irradiation')
    else:
      plt.hist(onState265k2p5Vpres, bins=50, range=(1.853, 1.90), color='black', alpha=0.5, label='Pre Gamma Irradiation')
      plt.hist(onState265k2p5Vposts, bins=50, range=(1.853, 1.90), color='red', alpha=0.5, label='Post Gamma Irradiation')

    # Plot editing
    if self.__batchType == 'Production':
      plt.xlim([1.785,1.88])
    else:
      plt.xlim([1.853,1.9])
    xmin, xmax, ymin, ymax = plt.axis()
    if largeFont:
      plt.xlabel(r'$\mathregular{I_{D}}$ at $\mathregular{V_{GS}=2.5\,V}$ [mA]', fontsize=22)
      plt.ylabel(r'Number of GaNFETs', fontsize=22)
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.9+ymin, "ATLAS ITk", fontstyle='italic', fontweight='bold', fontsize=22)
      plt.text((xmax-xmin)*0.24+xmin, (ymax-ymin)*0.9+ymin, "Strip", fontstyle='italic', fontsize=22)
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.83+ymin, "{} Batch Sample".format(self.__batchType), fontsize=22)
      if self.__batchType == 'Production':
        plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.76+ymin, "Data from 76/80 GaNFETs", fontsize=22)
      else:
        plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.76+ymin, "Data from 80/80 GaNFETs", fontsize=22)
      plt.legend(loc=(0.03, 0.59), ncol=1, numpoints=5, fontsize=22).get_frame().set_linewidth(0.0)
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.545+ymin, r"$\mathregular{(\overline{x}\,\pm\,\sigma)_{pre}}$"+r" = ({:.3f}".format(preMean)+r"$\,\mathregular{\pm}\,$"+r"{:.3f})$\,$mA".format(preStdev), fontsize=22)
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.475+ymin, r"$\mathregular{(\overline{x}\,\pm\,\sigma)_{post}}$"+r" = ({:.3f}".format(postMean)+r"$\,\mathregular{\pm}\,$"+r"{:.3f})$\,$mA".format(postStdev), fontsize=22)
      plt.yticks(fontsize=22)
      plt.xticks(fontsize=22)
    else:
      plt.xlabel(r'$\mathregular{I_{D}}$ at $\mathregular{V_{GS}=2.5\,V}$ [mA]')
      plt.ylabel(r'Number of GaNFETs')
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.90+ymin, "ATLAS ITk", fontstyle='italic', fontweight='bold')
      plt.text((xmax-xmin)*0.205+xmin, (ymax-ymin)*0.9+ymin, "Strip", fontstyle='italic')
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.84+ymin, "{} Batch Sample".format(self.__batchType))
      if self.__batchType == 'Production':
        plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.78+ymin, "Data from 76/80 GaNFETs")
      else:
        plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.78+ymin, "Data from 80/80 GaNFETs")
      plt.legend(loc=(0.04, 0.64), ncol=1, numpoints=5).get_frame().set_linewidth(0.0)
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.605+ymin, r"$\mathregular{(\overline{x}\,\pm\,\sigma)_{pre}}$"+r" = ({:.3f}".format(preMean)+r"$\,\mathregular{\pm}\,$"+r"{:.3f})$\,$mA".format(preStdev))
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.545+ymin, r"$\mathregular{(\overline{x}\,\pm\,\sigma)_{post}}$"+r" = ({:.3f}".format(postMean)+r"$\,\mathregular{\pm}\,$"+r"{:.3f})$\,$mA".format(postStdev))

    # Save figure
    try:
      plt.savefig('./../data/plots/'+fileName+".png", format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)
    except:
      os.system("mkdir ./../data/plots")
      plt.savefig('./../data/plots/'+fileName+".png", format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)

    # Close plot
    plt.close()

  def plotIdChangeAtVgs2p5VFromOnState265k(self, largeFont=False):
    # Activate ATLAS style
    ampl.use_atlas_style()

    # General variables
    fileName = "merged{}OnState265kIdChangeAtVgs2p5V".format(self.__batchType.replace("-",""))

    # Get relevant ganfet variables
    onState265k2p5Vscalings = []
    for fet in self.__ganfets:
      onState265k2p5Vpre,onState265k2p5Vpost,onState265k2p5Vscaling = fet.curr265kId()
      if (onState265k2p5Vpre != -99 or onState265k2p5Vpost != -99 or onState265k2p5Vscaling != -99) and (self.__batchType == 'Pre-production' or (self.__batchType == 'Production' and fet.num() not in [9, 20, 60, 70])):
        onState265k2p5Vscalings.append(onState265k2p5Vscaling*100)

    # Get distribution statistics
    mean = statistics.mean(onState265k2p5Vscalings)
    stdev = statistics.stdev(onState265k2p5Vscalings)

    # Plot
    if self.__batchType == 'Production':
      plt.hist(onState265k2p5Vscalings, bins=25, range=(-4.85,-2.5), color='blue', alpha=0.5)
    else:
      plt.hist(onState265k2p5Vscalings, bins=25, range=(-0.245,1.5), color='blue', alpha=0.5)

    # Plot editing
    if self.__batchType == 'Production':
      plt.xlim([-4.85,-2.4])
      xShift = 0.0
    else:
      plt.xlim([-0.245,1.5])
      xShift = 0.41
    xmin, xmax, ymin, ymax = plt.axis()
    if largeFont:
      plt.xlabel(r'$\mathregular{I_{D}}$ change at $\mathregular{V_{GS}=2.5}$ V [%]', fontsize=22)
      plt.ylabel(r'Number of GaNFETs', fontsize=22)
      plt.text((xmax-xmin)*(0.05+xShift)+xmin, (ymax-ymin)*0.9+ymin, "ATLAS ITk", fontstyle='italic', fontweight='bold', fontsize=22)
      plt.text((xmax-xmin)*(0.24+xShift)+xmin, (ymax-ymin)*0.9+ymin, "Strip", fontstyle='italic', fontsize=22)
      plt.text((xmax-xmin)*(0.05+xShift)+xmin, (ymax-ymin)*0.83+ymin, "{} Batch Sample".format(self.__batchType), fontsize=22)
      if self.__batchType == 'Production':
        plt.text((xmax-xmin)*(0.05+xShift)+xmin, (ymax-ymin)*0.76+ymin, "Data from 76/80 GaNFETs", fontsize=22)
      else:
        plt.text((xmax-xmin)*(0.05+xShift)+xmin, (ymax-ymin)*0.76+ymin, "Data from 80/80 GaNFETs", fontsize=22)
      plt.text((xmax-xmin)*(0.05+xShift)+xmin, (ymax-ymin)*0.69+ymin, r"$\mathregular{\overline{x}\,\pm\,\sigma}$"+r" = ({:.2f}".format(mean)+r"$\,\mathregular{\pm}\,$"+r"{:.2f})%".format(stdev), fontsize=22)
      plt.yticks(fontsize=22)
      plt.xticks(fontsize=22)
    else:
      plt.xlabel(r'$\mathregular{I_{D}}$ change at $\mathregular{V_{GS}=2.5}$ V [%]')
      plt.ylabel(r'Number of GaNFETs')
      plt.text((xmax-xmin)*(0.05+xShift)+xmin, (ymax-ymin)*0.90+ymin, "ATLAS ITk", fontstyle='italic', fontweight='bold')
      plt.text((xmax-xmin)*(0.205+xShift)+xmin, (ymax-ymin)*0.9+ymin, "Strip", fontstyle='italic')
      plt.text((xmax-xmin)*(0.05+xShift)+xmin, (ymax-ymin)*0.84+ymin, "{} Batch Sample".format(self.__batchType))
      if self.__batchType == 'Production':
        plt.text((xmax-xmin)*(0.05+xShift)+xmin, (ymax-ymin)*0.78+ymin, "Data from 76/80 GaNFETs")
      else:
        plt.text((xmax-xmin)*(0.05+xShift)+xmin, (ymax-ymin)*0.78+ymin, "Data from 80/80 GaNFETs")
      plt.text((xmax-xmin)*(0.05+xShift)+xmin, (ymax-ymin)*0.72+ymin, r"$\mathregular{\overline{x}\,\pm\,\sigma}$"+r" = ({:.2f}".format(mean)+r"$\,\mathregular{\pm}\,$"+r"{:.2f})%".format(stdev))

    # Save figure
    try:
      plt.savefig('./../data/plots/'+fileName+".png", format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)
    except:
      os.system("mkdir ./../data/plots")
      plt.savefig('./../data/plots/'+fileName+".png", format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)

    # Close plot
    plt.close()

  def plotIgFromOnState265k(self, largeFont=False):
    # Activate ATLAS style
    ampl.use_atlas_style()

    # General variables
    fileName = 'merged{}OnState265kIG'.format(self.__batchType.replace("-",""))

    # Get and plot data
    for fet in self.__ganfets:
      prefix = 'ganfet '+str(fet.num())+' '

      # Get pre and post irradiation data
      xPre = fet.onState265kPreIrrad()[prefix+'VGS'].to_numpy()
      yPre = fet.onState265kPreIrrad()[prefix+'IG'].to_numpy()*1e-6
      xPost = fet.onState265kPostIrrad()[prefix+'VGS'].to_numpy()
      yPost = fet.onState265kPostIrrad()[prefix+'IG'].to_numpy()*1e-6

      # Plot pre irradiation data
      if fet.num() == 1:
        plt.plot(xPre, yPre, color='black', marker='.', markersize=3, alpha = 0.3, label="Pre Gamma Irradiation")
        plt.plot(xPost, yPost, color='red', marker='.', markersize=3, alpha = 0.3, label="Post Gamma Irradiation")
      else:
        plt.plot(xPre, yPre, color='black', marker='.', markersize=3, alpha = 0.3)
        plt.plot(xPost, yPost, color='red', marker='.', markersize=3, alpha = 0.3)

    # Edit plotting
    plt.ticklabel_format(axis='y', style='sci', scilimits=(0,0))
    plt.xlim([0,2.5])
    if self.__batchType == 'Production':
      plt.ylim([-220,230])
    else:
      plt.ylim([-220,180])
    xmin, xmax, ymin, ymax = plt.axis()
    if largeFont:
      plt.xlabel(r'$\mathregular{V_{GS}}$ [V]', fontsize=22)
      plt.ylabel(r'$\mathregular{I_{G}}$ using 265.4$\,$k$\mathregular{\Omega}$ [$\mu$A]', fontsize=22)
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.9+ymin, "ATLAS ITk", fontstyle='italic', fontweight='bold', fontsize=22)
      plt.text((xmax-xmin)*0.24+xmin, (ymax-ymin)*0.9+ymin, "Strip", fontstyle='italic', fontsize=22)
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.83+ymin, "{} Batch Sample".format(self.__batchType), fontsize=22)
      if self.__batchType == 'Production':
        plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.76+ymin, "Pre data from 80/80 GaNFETs", fontsize=22)
        plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.69+ymin, "Post data from 79/80 GaNFETs", fontsize=22)
        plt.legend(loc=(0.03, 0.52), ncol=1, numpoints=5, fontsize=22).get_frame().set_linewidth(0.0)
      else:
        plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.76+ymin, "Data from 80/80 GaNFETs", fontsize=22)
        plt.legend(loc=(0.03, 0.59), ncol=1, numpoints=5, fontsize=22).get_frame().set_linewidth(0.0)
      plt.yticks(fontsize=22)
      plt.xticks(fontsize=22)
    else:
      plt.xlabel(r'$\mathregular{V_{GS}}$ [V]')
      plt.ylabel(r'$\mathregular{I_{G}}$ using 265.4$\,$k$\mathregular{\Omega}$ [$\mu$A]')
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.90+ymin, "ATLAS ITk", fontstyle='italic', fontweight='bold')
      plt.text((xmax-xmin)*0.205+xmin, (ymax-ymin)*0.9+ymin, "Strip", fontstyle='italic')
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.84+ymin, "{} Batch Sample".format(self.__batchType))
      if self.__batchType == 'Production':
        plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.78+ymin, "Pre data from 80/80 GaNFETs")
        plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.72+ymin, "Post data from 79/80 GaNFETs")
        plt.legend(loc=(0.04, 0.58), ncol=1, numpoints=5).get_frame().set_linewidth(0.0)
      else:
        plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.78+ymin, "Data from 80/80 GaNFETs")
        plt.legend(loc=(0.04, 0.64), ncol=1, numpoints=5).get_frame().set_linewidth(0.0)

    # Save figure
    try:
      plt.savefig('./../data/plots/'+fileName+".png", format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)
    except:
      os.system("mkdir ./../data/plots")
      plt.savefig('./../data/plots/'+fileName+".png", format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)

    # Close plot
    plt.close()

  def plotIsFromOnState265k(self, largeFont=False):
    # Activate ATLAS style
    ampl.use_atlas_style()

    # General variables
    fileName = 'merged{}OnState265kIS'.format(self.__batchType.replace("-",""))

    # Get and plot data
    for fet in self.__ganfets:
      prefix = 'ganfet '+str(fet.num())+' '

      # Get pre and post irradiation data
      xPre = fet.onState265kPreIrrad()[prefix+'VGS'].to_numpy()
      yPre = fet.onState265kPreIrrad()[prefix+'IS'].to_numpy()*1e-9
      xPost = fet.onState265kPostIrrad()[prefix+'VGS'].to_numpy()
      yPost = fet.onState265kPostIrrad()[prefix+'IS'].to_numpy()*1e-9

      # Plot pre irradiation data
      if fet.num() == 1:
        plt.plot(xPre, yPre, color='black', marker='.', markersize=3, alpha = 0.3, label="Pre Gamma Irradiation")
        plt.plot(xPost, yPost, color='red', marker='.', markersize=3, alpha = 0.3, label="Post Gamma Irradiation")
      else:
        plt.plot(xPre, yPre, color='black', marker='.', markersize=3, alpha = 0.3)
        plt.plot(xPost, yPost, color='red', marker='.', markersize=3, alpha = 0.3)

    # Edit plotting
    plt.ticklabel_format(axis='y', style='sci', scilimits=(0,0))
    plt.xlim([0,2.5])
    if self.__batchType == 'Production':
      plt.ylim([-0.2,4.0])
    else:
      plt.ylim([-0.2,3.5])
    xmin, xmax, ymin, ymax = plt.axis()
    if largeFont:
      plt.xlabel(r'$\mathregular{V_{GS}}$ [V]', fontsize=22)
      plt.ylabel(r'$\mathregular{I_{S}}$ using 265.4$\,$k$\mathregular{\Omega}$ [mA]', fontsize=22)
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.9+ymin, "ATLAS ITk", fontstyle='italic', fontweight='bold', fontsize=22)
      plt.text((xmax-xmin)*0.24+xmin, (ymax-ymin)*0.9+ymin, "Strip", fontstyle='italic', fontsize=22)
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.83+ymin, "{} Batch Sample".format(self.__batchType), fontsize=22)
      if self.__batchType == 'Production':
        plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.76+ymin, "Pre data from 80/80 GaNFETs", fontsize=22)
        plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.69+ymin, "Post data from 79/80 GaNFETs", fontsize=22)
        plt.legend(loc=(0.03, 0.52), ncol=1, numpoints=5, fontsize=22).get_frame().set_linewidth(0.0)
      else:
        plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.76+ymin, "Data from 80/80 GaNFETs", fontsize=22)
        plt.legend(loc=(0.03, 0.59), ncol=1, numpoints=5, fontsize=22).get_frame().set_linewidth(0.0)
      plt.yticks(fontsize=22)
      plt.xticks(fontsize=22)
    else:
      plt.xlabel(r'$\mathregular{V_{GS}}$ [V]')
      plt.ylabel(r'$\mathregular{I_{S}}$ using 265.4$\,$k$\mathregular{\Omega}$ [mA]')
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.90+ymin, "ATLAS ITk", fontstyle='italic', fontweight='bold')
      plt.text((xmax-xmin)*0.205+xmin, (ymax-ymin)*0.9+ymin, "Strip", fontstyle='italic')
      plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.84+ymin, "{} Batch Sample".format(self.__batchType))
      if self.__batchType == 'Production':
        plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.78+ymin, "Pre data from 80/80 GaNFETs")
        plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.72+ymin, "Post data from 79/80 GaNFETs")
        plt.legend(loc=(0.04, 0.58), ncol=1, numpoints=5).get_frame().set_linewidth(0.0)
      else:
        plt.text((xmax-xmin)*0.05+xmin, (ymax-ymin)*0.78+ymin, "Data from 80/80 GaNFETs")
        plt.legend(loc=(0.04, 0.64), ncol=1, numpoints=5).get_frame().set_linewidth(0.0)
    
    # Save figure
    try:
      plt.savefig('./../data/plots/'+fileName+".png", format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)
    except:
      os.system("mkdir ./../data/plots")
      plt.savefig('./../data/plots/'+fileName+".png", format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)

    # Close plot
    plt.close()

