import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.interpolate import make_interp_spline
import atlas_mpl_style as ampl
import os

class ganfet():
  
  def __init__(self, batchType, number):
    self.__batchType = batchType
    self.__number = number
  
  def num(self):
    return self.__number
  
  def setVgsIdRampPreIrrad(self, df):
    df = df.fillna(-999)
    self.__vgsIdRampPreIrradDF = df

  def vgsIdRampPreIrrad(self):
    return self.__vgsIdRampPreIrradDF
  
  def setOffStatePreIrrad(self, df):
    df = df.fillna(-999)
    self.__offStatePreIrradDF = df
  
  def offStatePreIrrad(self):
    return self.__offStatePreIrradDF
  
  def setOnState1GigPreIrrad(self, df):
    df = df.fillna(-999)
    self.__onState1GigPreIrradDF = df
  
  def onState1GigPreIrrad(self):
    return self.__onState1GigPreIrradDF
  
  def setOnState265kPreIrrad(self, df):
    df = df.fillna(-999)
    self.__onState265kPreIrradDF = df
  
  def onState265kPreIrrad(self):
    return self.__onState265kPreIrradDF
  
  def setVgsIdRampPostIrrad(self, df):
    df = df.fillna(-999)
    self.__vgsIdRampPostIrradDF = df
  
  def vgsIdRampPostIrrad(self):
    return self.__vgsIdRampPostIrradDF
  
  def setOffStatePostIrrad(self, df):
    df = df.fillna(-999)
    self.__offStatePostIrradDF = df
  
  def offStatePostIrrad(self):
    return self.__offStatePostIrradDF
  
  def setOnState1GigPostIrrad(self, df):
    df = df.fillna(-999)
    self.__onState1GigPostIrradDF = df
  
  def onState1GigPostIrrad(self):
    return self.__onState1GigPostIrradDF
  
  def setOnState265kPostIrrad(self, df):
    df = df.fillna(-999)
    self.__onState265kPostIrradDF = df
  
  def onState265kPostIrrad(self):
    return self.__onState265kPostIrradDF

  def plotVgsIdRamp(self):
    # Activate ATLAS style
    ampl.use_atlas_style()
    
    # General variables
    prefix = 'ganfet '+str(self.__number)+' '
    fileName = 'ganfet'+str(self.__number)+str(self.__batchType).replace("-","")+'VgsIdRamp'
    
    # Extract x-axis and y-axis information for pre and post irradiation
    xPre = self.__vgsIdRampPreIrradDF[prefix+'VGS'].to_numpy()
    xPost = self.__vgsIdRampPostIrradDF[prefix+'VGS'].to_numpy()
    yPre = self.__vgsIdRampPreIrradDF[prefix+'IDS'].to_numpy()
    yPost = self.__vgsIdRampPostIrradDF[prefix+'IDS'].to_numpy()
    
    ###############################
    # Calculate voltage threshold #
    ###############################

    # Calculate middle current used for voltage threshold
    maxCurrPre = max(yPre)
    minCurrPre = min(yPre)
    midCurrPre = (maxCurrPre-minCurrPre)/2.0

    # Note that the x-axis of the pre and post is different.
    # We need to make independent loops

    # Determine the pre-irradiation voltage threshold
    minDiffPre = 999
    vClosestMidCurrPre = -1
    for i in range(len(yPre)):
      curr = yPre[i]
      diff = abs(curr-midCurrPre)
      if diff > minDiffPre+0.05: # We have clearly passed the mid current
        break
      if diff < minDiffPre:
        minDiffPre = diff
        vClosestMidCurrPre = xPre[i]
    
    # Determine the post-irradiation voltage threshold
    minDiffPost = 999
    vClosestMidCurrPost = -1
    for i in range(len(yPost)):
      curr = yPost[i]
      diff = abs(curr-midCurrPre)
      if diff > minDiffPost+0.05: # We have clearly passed the mid current
        break
      if diff < minDiffPost:
        minDiffPost = diff
        vClosestMidCurrPost = xPost[i]
    
    # Set values
    self.__vThPre = vClosestMidCurrPre
    self.__vThPost = vClosestMidCurrPost
    self.__vThShift = self.__vThPost - self.__vThPre

    # Plot
    plt.plot(xPre, yPre, color='black', marker='.', markersize=3, label="Pre Gamma Irradiation")
    plt.plot(xPost, yPost, color='red', marker='.', markersize=3, label="Post Gamma Irradiation")
    plt.axhline(y=midCurrPre, color='grey', linestyle='dotted')
    plt.xlim([0,3.5])
    plt.ylim([-0.1,2])
    plt.ylabel(r'$\mathregular{I_{D}}$ [A]')
    plt.xlabel(r'$\mathregular{V_{GS}}$ [V]')
    xMin, xMax, yMin, yMax = plt.axis()
    if self.__batchType == 'Pre-production':
      xShift = 0.5
    else:
      xShift = 0.0
    plt.text((xMax-xMin)*(0.05+xShift)+xMin, (yMax-yMin)*0.90+yMin, "ATLAS ITk", fontstyle='italic', fontweight='bold')
    plt.text((xMax-xMin)*(0.205+xShift)+xMin, (yMax-yMin)*0.90+yMin, "Strip", fontstyle='italic')
    plt.text((xMax-xMin)*(0.05+xShift)+xMin, (yMax-yMin)*0.84+yMin, "{} Batch Sample".format(self.__batchType))
    plt.text((xMax-xMin)*(0.05+xShift)+xMin, (yMax-yMin)*0.78+yMin, r'GaNFET '+str(self.__number))
    plt.text((xMax-xMin)*(0.05)+xMin, midCurrPre+(yMax-yMin)*0.02, r"$I_{D}=$"+str(round(midCurrPre,3))+r"$\,$A" "\n" r"$V_{th, shift}=$"+str(round(self.__vThShift,3))+r"$\,$V")
    plt.legend(loc=(0.04+xShift, 0.64), ncol=1, numpoints=5).get_frame().set_linewidth(0.0)
    try:
      plt.savefig('./../data/plots/'+fileName+'.png', format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)
      pass
    except:
      os.system("mkdir ./../data/plots")
      plt.savefig('./../data/plots/'+fileName+'.png', format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)
      pass
    plt.close()
  
  def plotVgsIdRampVDS(self):
    # Activate ATLAS style
    ampl.use_atlas_style()

    # General variables
    prefix = 'ganfet '+str(self.__number)+' '
    fileName = 'ganfet'+str(self.__number)+str(self.__batchType).replace("-","")+'VgsIdRampVDS'

    # Get data 
    xPre = self.__vgsIdRampPreIrradDF[prefix+'VGS'].to_numpy()
    yPre = self.__vgsIdRampPreIrradDF[prefix+'VDS'].to_numpy()
    xPost = self.__vgsIdRampPostIrradDF[prefix+'VGS'].to_numpy()
    yPost = self.__vgsIdRampPostIrradDF[prefix+'VDS'].to_numpy()

    # Plot
    plt.plot(xPre, yPre, color='black', marker='.', markersize=3, label="Pre Gamma Irradiation")
    plt.plot(xPost, yPost, color='red', marker='.', markersize=3, label="Post Gamma Irradiation")
    plt.xlim([0,3.5])
    plt.ylim([-0.5,11])
    plt.xlabel(r'$\mathregular{V_{GS}}$ [V]')
    plt.ylabel(r'$\mathregular{V_{DS}}$ [V]')
    xMin, xMax, yMin, yMax = plt.axis()
    plt.text((xMax-xMin)*0.56+xMin, (yMax-yMin)*0.90+yMin, "ATLAS ITk", fontstyle='italic', fontweight='bold')
    plt.text((xMax-xMin)*0.715+xMin, (yMax-yMin)*0.90+yMin, "Strip", fontstyle='italic')
    plt.text((xMax-xMin)*0.56+xMin, (yMax-yMin)*0.84+yMin, r"{} Batch Sample".format(self.__batchType))
    plt.text((xMax-xMin)*0.56+xMin, (yMax-yMin)*0.78+yMin, r'GaNFET '+str(self.__number))
    plt.legend(loc=(0.55, 0.64), ncol=1, numpoints=5).get_frame().set_linewidth(0.0)
    try:
      plt.savefig('./../data/plots/'+fileName+'.png', format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)
      pass
    except:
      os.system("mkdir ./../data/plots")
      plt.savefig('./../data/plots/'+fileName+'.png', format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)
      pass
    plt.close()

  def plotVgsIdRampVsupply(self):
    # Activate ATLAS style
    ampl.use_atlas_style()

    # General variables
    prefix = 'ganfet '+str(self.__number)+' '
    fileName = 'ganfet'+str(self.__number)+str(self.__batchType).replace("-","")+'VgsIdRampVsupply'
    
    # Get data
    xPre = self.__vgsIdRampPreIrradDF[prefix+'VGS'].to_numpy()
    yPre = self.__vgsIdRampPreIrradDF[prefix+'Vsupply'].to_numpy()
    xPost = self.__vgsIdRampPostIrradDF[prefix+'VGS'].to_numpy()
    yPost = self.__vgsIdRampPostIrradDF[prefix+'Vsupply'].to_numpy()
    
    # Plot
    plt.plot(xPre, yPre, color='black', marker='.', markersize=3, label="Pre Gamma Irradiation")
    plt.plot(xPost, yPost, color='red', marker='.', markersize=3, label="Post Gamma Irradiation")
    plt.xlim([0,3.5])
    plt.ylim([8.9,11])
    plt.xlabel(r'$\mathregular{V_{GS}}$ [V]')
    plt.ylabel(r'$\mathregular{V_{supply}}$ [V]')
    xMin, xMax, yMin, yMax = plt.axis()
    plt.text((xMax-xMin)*0.05+xMin, (yMax-yMin)*0.90+yMin, "ATLAS ITk", fontstyle='italic', fontweight='bold')
    plt.text((xMax-xMin)*0.205+xMin, (yMax-yMin)*0.90+yMin, "Strip", fontstyle='italic')
    plt.text((xMax-xMin)*0.05+xMin, (yMax-yMin)*0.84+yMin, r"{} Batch Sample".format(self.__batchType))
    plt.text((xMax-xMin)*0.05+xMin, (yMax-yMin)*0.78+yMin, r'GaNFET '+str(self.__number))
    plt.legend(loc=(0.04, 0.64), ncol=1, numpoints=5).get_frame().set_linewidth(0.0)
    try:
      plt.savefig('./../data/plots/'+fileName+'.png', format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)
      pass
    except:
      os.system("mkdir ./../data/plots")
      plt.savefig('./../data/plots/'+fileName+'.png', format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)
      pass
    plt.close()

  def plotOffState(self, log=False):
    # Activate ATLAS style
    ampl.use_atlas_style()

    # General variables
    prefix = 'ganfet '+str(self.__number)+' '
    fileName = 'ganfet'+str(self.__number)+str(self.__batchType).replace("-","")+'OffState'
    
    # Get data
    xPre = self.__offStatePreIrradDF[prefix+'VGS'].to_numpy()
    yPre = self.__offStatePreIrradDF[prefix+'ID'].to_numpy()
    xPost = self.__offStatePostIrradDF[prefix+'VGS'].to_numpy()
    yPost = self.__offStatePostIrradDF[prefix+'ID'].to_numpy()

    # Plot
    plt.plot(xPre, yPre, color='black', marker='.', markersize=3, label="Pre Gamma Irradiation")
    plt.plot(xPost, yPost, color='red', marker='.', markersize=3, label="Post Gamma Irradiation")

    # Get relevant values
    desiredVds = 550
    self.__idAt550Pre = float(yPre[-1])
    self.__idAt550Post = float(yPost[-1])
    if self.__idAt550Pre != 0:
      self.__idAt550Scaling = (self.__idAt550Post-self.__idAt550Pre)/self.__idAt550Pre
    else:
      self.__idAt550Scaling = -99
    desiredVds2 = 250
    if self.__batchType == 'Production':
      self.__idAt250Pre = float(yPre[11])
      self.__idAt250Post = float(yPost[11])
    else:
      self.__idAt250Pre = float(yPre[4])
      self.__idAt250Post = float(yPost[4])
    if self.__idAt250Pre != 0:
      self.__idAt250Scaling = (self.__idAt250Post-self.__idAt250Pre)/self.__idAt250Pre
    else:
      self.__idAt250Scaling = -99

    # Edit plotting
    plt.ticklabel_format(axis='y', style='sci', scilimits=(0,0))
    plt.xlim([0,600])
    if log and -999 not in xPre:
      plt.yscale("log")
    else:
      xMin, xMax, yMin, yMax = plt.axis()
      plt.ylim([-0.4e5, yMax*1.05])
    xMin, xMax, yMin, yMax = plt.axis()
    plt.xlabel(r'$\mathregular{V_{DS}}$ [V]')
    plt.ylabel(r'$\mathregular{I_{D}}$ [pA]')
    try:
      plt.text(0.915, 0.05, r"$V_{DS} = $"+str(desiredVds)+r"$\,$V" "\n" r"Increase = "+str(int(round(self.__idAt550Scaling*100,0)))+r"$\,$%", horizontalalignment='right', transform=plt.gca().transAxes)
      plt.axvline(x=desiredVds, color='grey', linestyle='dotted')
      plt.text(0.411, 0.05, r"$V_{DS} = $"+str(desiredVds2)+r"$\,$V" "\n" r"Increase = "+str(int(round(self.__idAt250Scaling*100,0)))+r"$\,$%", horizontalalignment='right', transform=plt.gca().transAxes)
      plt.axvline(x=desiredVds2, color='grey', linestyle='dotted')
    except:
      pass
    plt.text(0.05, 0.90, "ATLAS ITk", fontstyle='italic', fontweight='bold', transform=plt.gca().transAxes)
    plt.text(0.205, 0.90, "Strip", fontstyle='italic', transform=plt.gca().transAxes)
    plt.text(0.05, 0.84, r"{} Batch Sample".format(self.__batchType), transform=plt.gca().transAxes)
    plt.text(0.05, 0.78, r'GaNFET '+str(self.__number), transform=plt.gca().transAxes)
    plt.legend(loc=(0.04, 0.64), ncol=1, numpoints=5).get_frame().set_linewidth(0.0)
    try:
      if log:
        plt.savefig('./../data/plots/'+fileName+"Log", dpi=200)
        plt.savefig('./../data/plots/'+fileName+"Log.pdf", dpi=200)
        plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)
        pass
      else:
        plt.savefig('./../data/plots/'+fileName+'.png', format='png', dpi=200)
        plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
        plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)
        pass
    except:
      os.system("mkdir ./../data/plots")
      if log:
        plt.savefig('./../data/plots/'+fileName+"Log", dpi=200)
        plt.savefig('./../data/plots/'+fileName+"Log.pdf", dpi=200)
        plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)
        pass
      else:
        plt.savefig('./../data/plots/'+fileName+'.png', format='png', dpi=200)
        plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
        plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)
        pass
    plt.close()

  def plotOffStateIG(self):
    # Activate ATLAS style
    ampl.use_atlas_style()

    # General variables
    prefix = 'ganfet '+str(self.__number)+' '
    fileName = 'ganfet'+str(self.__number)+str(self.__batchType).replace("-","")+'OffStateIG'

    # Get data
    xPre = self.__offStatePreIrradDF[prefix+'VGS'].to_numpy()
    yPre = self.__offStatePreIrradDF[prefix+'IG'].to_numpy()
    xPost = self.__offStatePostIrradDF[prefix+'VGS'].to_numpy()
    yPost = self.__offStatePostIrradDF[prefix+'IG'].to_numpy()

    # Plot
    plt.plot(xPre, yPre, color='black', marker='.', markersize=3, label="Pre Gamma Irradiation")
    plt.plot(xPost, yPost, color='red', marker='.', markersize=3, label="Post Gamma Irradiation")
    plt.ticklabel_format(axis='y', style='sci', scilimits=(0,0))
    xMin, xMax, yMin, yMax = plt.axis()
    plt.xlim([0,600])
    plt.ylim([(yMax-yMin)*0.05+yMin, yMax*1.05])
    plt.xlabel(r'$\mathregular{V_{DS}}$ [V]')
    plt.ylabel(r'$\mathregular{I_{G}}$ [pA]')
    xMin, xMax, yMin, yMax = plt.axis()
    plt.text((xMax-xMin)*0.05+xMin, (yMax-yMin)*0.90+yMin, "ATLAS ITk", fontstyle='italic', fontweight='bold')
    plt.text((xMax-xMin)*0.205+xMin, (yMax-yMin)*0.90+yMin, "Strip", fontstyle='italic')
    plt.text((xMax-xMin)*0.05+xMin, (yMax-yMin)*0.84+yMin, r"{} Batch Sample".format(self.__batchType))
    plt.text((xMax-xMin)*0.05+xMin, (yMax-yMin)*0.78+yMin, r'GaNFET '+str(self.__number))
    plt.legend(loc=(0.04, 0.64), ncol=1, numpoints=5).get_frame().set_linewidth(0.0)
    try:
      plt.savefig('./../data/plots/'+fileName+'.png', format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)
      pass
    except:
      os.system("mkdir ./../data/plots")
      plt.savefig('./../data/plots/'+fileName+'.png', format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)
      pass
    plt.close()
  
  def plotOffStateDerivative(self, log=False):
    # Activate ATLAS style
    ampl.use_atlas_style()

    # General variables
    prefix = 'ganfet '+str(self.__number)+' '
    fileName = 'ganfet'+str(self.__number)+str(self.__batchType).replace("-","")+'OffStateDerivative'
    
    # Get data
    xPre = self.__offStatePreIrradDF[prefix+'VGS'].to_numpy()
    yPre = self.__offStatePreIrradDF[prefix+'ID'].to_numpy()
    xPost = self.__offStatePostIrradDF[prefix+'VGS'].to_numpy()
    yPost = self.__offStatePostIrradDF[prefix+'ID'].to_numpy()

    # Calculate slopes between points
    xPre2 = [(xPre[i+1]+xPre[i])/2 for i in range(len(xPre)-1)]
    yPre2 = [(yPre[i+1]-yPre[i])/(xPre[i+1]-xPre[i]) if (xPre[i+1]-xPre[i]) > 0 else -999 for i in range(len(xPre)-1)]
    xPost2 = [(xPost[i+1]+xPost[i])/2 for i in range(len(xPost)-1)]
    yPost2 = [(yPost[i+1]-yPost[i])/(xPost[i+1]-xPost[i]) if (xPost[i+1]-xPost[i]) > 0 else -999 for i in range(len(xPost)-1)]

    # Plot
    plt.plot(xPre2, yPre2, color='black', marker='.', markersize=3, label="Pre Gamma Irradiation")
    plt.plot(xPost2, yPost2, color='red', marker='.', markersize=3, label="Post Gamma Irradiation")

    # Edit plotting
    plt.ticklabel_format(axis='y', style='sci', scilimits=(0,0))
    plt.xlim([0,600])
    if log and -999 not in xPre:
      plt.yscale("log")
    '''
    else:
      xMin, xMax, yMin, yMax = plt.axis()
      plt.ylim([(yMax-yMin)*0.05+yMin, yMax*1.05])
    '''
    plt.xlabel(r'$\mathregular{V_{DS}}$ [V]')
    plt.ylabel(r'$\mathregular{\frac{\Delta I_{D}}{\Delta V_{DS}}}$ [pA/V]')
    plt.text(0.05, 0.90, "ATLAS ITk", fontstyle='italic', fontweight='bold', transform=plt.gca().transAxes)
    plt.text(0.205, 0.90, "Strip", fontstyle='italic', transform=plt.gca().transAxes)
    plt.text(0.05, 0.84, "{} Batch Sample".format(self.__batchType), transform=plt.gca().transAxes)
    plt.text(0.05, 0.78, r'GaNFET '+str(self.__number), transform=plt.gca().transAxes)
    plt.legend(loc=(0.04, 0.64), ncol=1, numpoints=5).get_frame().set_linewidth(0.0)
    try:
      if log:
        plt.savefig('./../data/plots/'+fileName+"Log", dpi=200)
        plt.savefig('./../data/plots/'+fileName+"Log.pdf", dpi=200)
        plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)
        pass
      else:
        plt.savefig('./../data/plots/'+fileName+'.png', format='png', dpi=200)
        plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
        plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)
        pass
    except:
      os.system("mkdir ./../data/plots")
      if log:
        plt.savefig('./../data/plots/'+fileName+"Log", dpi=200)
        plt.savefig('./../data/plots/'+fileName+"Log.pdf", dpi=200)
        plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)
        pass
      else:
        plt.savefig('./../data/plots/'+fileName+'.png', format='png', dpi=200)
        plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
        plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)
        pass
    plt.close()

  def plotOffStateIS(self):
    # Activate ATLAS style
    ampl.use_atlas_style()

    # General variables 
    prefix = 'ganfet '+str(self.__number)+' '
    fileName = 'ganfet'+str(self.__number)+str(self.__batchType).replace("-","")+'OffStateIS'

    # Get data
    xPre = self.__offStatePreIrradDF[prefix+'VGS'].to_numpy()
    yPre = self.__offStatePreIrradDF[prefix+'IS'].to_numpy()
    xPost = self.__offStatePostIrradDF[prefix+'VGS'].to_numpy()
    yPost = self.__offStatePostIrradDF[prefix+'IS'].to_numpy()

    # Plot
    plt.plot(xPre, yPre, color='black', marker='.', markersize=3, label="Pre Gamma Irradiation")
    plt.plot(xPost, yPost, color='red', marker='.', markersize=3, label="Post Gamma Irradiation")
    plt.ticklabel_format(axis='y', style='sci', scilimits=(0,0))
    xMin, xMax, yMin, yMax = plt.axis()
    plt.xlim([0,600])
    plt.ylim([(yMax-yMin)*0.05+yMin, yMax*1.05])
    plt.xlabel(r'$\mathregular{V_{DS}}$ [V]')
    plt.ylabel(r'$\mathregular{I_{S}}$ [pA]')
    xMin, xMax, yMin, yMax = plt.axis()
    plt.text((xMax-xMin)*0.05+xMin, (yMax-yMin)*0.90+yMin, "ATLAS ITk", fontstyle='italic', fontweight='bold')
    plt.text((xMax-xMin)*0.205+xMin, (yMax-yMin)*0.90+yMin, "Strip", fontstyle='italic')
    plt.text((xMax-xMin)*0.05+xMin, (yMax-yMin)*0.84+yMin, r"{} Batch Sample".format(self.__batchType))
    plt.text((xMax-xMin)*0.05+xMin, (yMax-yMin)*0.78+yMin, r'GaNFET '+str(self.__number))
    plt.legend(loc=(0.04, 0.64), ncol=1, numpoints=5).get_frame().set_linewidth(0.0)
    try:
      plt.savefig('./../data/plots/'+fileName+'.png', format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)
      pass
    except:
      os.system("mkdir ./../data/plots")
      plt.savefig('./../data/plots/'+fileName+'.png', format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)
      pass
    plt.close()

  def plotOnState1Gig(self):
    # Activate ATLAS style
    ampl.use_atlas_style()
    
    # General variables 
    prefix = 'ganfet '+str(self.__number)+' '
    fileName = 'ganfet'+str(self.__number)+str(self.__batchType).replace("-","")+'OnState1Gig'

    # Get data
    xPre = self.__onState1GigPreIrradDF[prefix+'VGS'].to_numpy()
    yPre = self.__onState1GigPreIrradDF[prefix+'ID=IS+IG'].to_numpy()
    xPost = self.__onState1GigPostIrradDF[prefix+'VGS'].to_numpy()
    yPost = self.__onState1GigPostIrradDF[prefix+'ID=IS+IG'].to_numpy()

    # Get relevant variables
    self.__id1Gpre = yPre[0]
    self.__id1Gpost = yPost[0]
    self.__id1Gscaling = (self.__id1Gpost-self.__id1Gpre)/self.__id1Gpre

    # Plot
    plt.plot(xPre, yPre, color='black', marker='.', markersize=3, label="Pre Gamma Irradiation")
    plt.plot(xPost, yPost, color='red', marker='.', markersize=3, label="Post Gamma Irradiation")
    plt.ticklabel_format(axis='y', style='sci', scilimits=(0,0))
    plt.xlim([0,2.5])
    plt.ylim([-0.4e5,9e5])
    xMin, xMax, yMin, yMax = plt.axis()
    plt.xlabel(r'$\mathregular{V_{GS}}$ [V]')
    plt.ylabel(r'$\mathregular{I_{D}}$ using 1G$\mathregular{\Omega}$ [pA]')
    plt.text((xMax-xMin)*0.05+xMin, (yMax-yMin)*0.90+yMin, "ATLAS ITk", fontstyle='italic', fontweight='bold')
    plt.text((xMax-xMin)*0.205+xMin, (yMax-yMin)*0.90+yMin, "Strip", fontstyle='italic')
    plt.text((xMax-xMin)*0.05+xMin, (yMax-yMin)*0.84+yMin, r"{} Batch Sample".format(self.__batchType))
    plt.text((xMax-xMin)*0.05+xMin, (yMax-yMin)*0.78+yMin, r'GaNFET '+str(self.__number))
    plt.legend(loc=(0.04, 0.64), ncol=1, numpoints=5).get_frame().set_linewidth(0.0)
    try:
      if self.__id1Gscaling*100 > 500:
        plt.text((xMax-xMin)*0.05+xMin, (yMax-yMin)*0.605+yMin, r"$Z_{off}\,\ll\,1\,G\Omega$")
      else:
        plt.text((xMax-xMin)*0.05+xMin, (yMax-yMin)*0.605+yMin, r"$Increase\,=\,$"+str(int(self.__id1Gscaling*100))+r"$\,\%\ at\ V_{GS}\,=\,0\,V$")
    except:
      pass
    try:
      plt.savefig('./../data/plots/'+fileName+'.png', format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)
      pass
    except:
      os.system("mkdir ./../data/plots")
      plt.savefig('./../data/plots/'+fileName+'.png', format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)
      pass
    plt.close()

  def plotOnState1GigIG(self):
    # Activate ATLAS style
    ampl.use_atlas_style()
    
    # General variables 
    prefix = 'ganfet '+str(self.__number)+' '
    fileName = 'ganfet'+str(self.__number)+str(self.__batchType).replace("-","")+'OnState1GigIG'

    # Get data
    xPre = self.__onState1GigPreIrradDF[prefix+'VGS'].to_numpy()
    yPre = self.__onState1GigPreIrradDF[prefix+'IG'].to_numpy()
    xPost = self.__onState1GigPostIrradDF[prefix+'VGS'].to_numpy()
    yPost = self.__onState1GigPostIrradDF[prefix+'IG'].to_numpy()

    # Plot
    plt.plot(xPre, yPre, color='black', marker='.', markersize=3, label="Pre Gamma Irradiation")
    plt.plot(xPost, yPost, color='red', marker='.', markersize=3, label="Post Gamma Irradiation")
    plt.ticklabel_format(axis='y', style='sci', scilimits=(0,0))
    plt.xlim([0,2.5])
    xMin, xMax, yMin, yMax = plt.axis()
    plt.ylim([(yMax-yMin)*0.07+yMin, (yMax-yMin)*1.4+yMin])
    plt.xlabel(r'$\mathregular{V_{GS}}$ [V]')
    plt.ylabel(r'$\mathregular{I_{G}}$ using 1G$\mathregular{\Omega}$ [pA]')
    xMin, xMax, yMin, yMax = plt.axis()
    plt.text((xMax-xMin)*0.05+xMin, (yMax-yMin)*0.90+yMin, "ATLAS ITk", fontstyle='italic', fontweight='bold')
    plt.text((xMax-xMin)*0.205+xMin, (yMax-yMin)*0.90+yMin, "Strip", fontstyle='italic')
    plt.text((xMax-xMin)*0.05+xMin, (yMax-yMin)*0.84+yMin, r"{} Batch Sample".format(self.__batchType))
    plt.text((xMax-xMin)*0.05+xMin, (yMax-yMin)*0.78+yMin, r'GaNFET '+str(self.__number))
    plt.legend(loc=(0.04, 0.64), ncol=1, numpoints=5).get_frame().set_linewidth(0.0)
    try:
      plt.savefig('./../data/plots/'+fileName+'.png', format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)
      pass
    except:
      os.system("mkdir ./../data/plots")
      plt.savefig('./../data/plots/'+fileName+'.png', format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)
      pass
    plt.close()

  def plotOnState1GigIS(self):
    # Activate ATLAS style
    ampl.use_atlas_style()
    
    # General variables 
    prefix = 'ganfet '+str(self.__number)+' '
    fileName = 'ganfet'+str(self.__number)+str(self.__batchType).replace("-","")+'OnState1GigIS'

    # Get data
    xPre = self.__onState1GigPreIrradDF[prefix+'VGS'].to_numpy()
    yPre = self.__onState1GigPreIrradDF[prefix+'IS'].to_numpy()
    xPost = self.__onState1GigPostIrradDF[prefix+'VGS'].to_numpy()
    yPost = self.__onState1GigPostIrradDF[prefix+'IS'].to_numpy()

    # Plot
    plt.plot(xPre, yPre, color='black', marker='.', markersize=3, label="Pre Gamma Irradiation")
    plt.plot(xPost, yPost, color='red', marker='.', markersize=3, label="Post Gamma Irradiation")
    plt.ticklabel_format(axis='y', style='sci', scilimits=(0,0))
    plt.xlim([0,2.5])
    xMin, xMax, yMin, yMax = plt.axis()
    plt.ylim([(yMax-yMin)*0.05+yMin, (yMax-yMin)*1.33+yMin])
    plt.xlabel(r'$\mathregular{V_{GS}}$ [V]')
    plt.ylabel(r'$\mathregular{I_{S}}$ using 1G$\mathregular{\Omega}$ [pA]')
    xMin, xMax, yMin, yMax = plt.axis()
    plt.text((xMax-xMin)*0.05+xMin, (yMax-yMin)*0.90+yMin, "ATLAS ITk", fontstyle='italic', fontweight='bold')
    plt.text((xMax-xMin)*0.205+xMin, (yMax-yMin)*0.90+yMin, "Strip", fontstyle='italic')
    plt.text((xMax-xMin)*0.05+xMin, (yMax-yMin)*0.84+yMin, r"{} Batch Sample".format(self.__batchType))
    plt.text((xMax-xMin)*0.05+xMin, (yMax-yMin)*0.78+yMin, r'GaNFET '+str(self.__number))
    plt.legend(loc=(0.04, 0.64), ncol=1, numpoints=5).get_frame().set_linewidth(0.0)
    try:
      plt.savefig('./../data/plots/'+fileName+'.png', format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)
      pass
    except:
      os.system("mkdir ./../data/plots")
      plt.savefig('./../data/plots/'+fileName+'.png', format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)
      pass
    plt.close()

  def plotOnState265k(self):
    # Activate ATLAS style
    ampl.use_atlas_style()

    # General variables 
    prefix = 'ganfet '+str(self.__number)+' '
    fileName = 'ganfet'+str(self.__number)+str(self.__batchType).replace("-","")+'OnState265k'

    # Get data
    xPre = self.__onState265kPreIrradDF[prefix+'VGS'].to_numpy()
    yPre = self.__onState265kPreIrradDF[prefix+'ID=IS+IG'].to_numpy()
    yPost = self.__onState265kPostIrradDF[prefix+'ID=IS+IG'].to_numpy()
    xPost = self.__onState265kPostIrradDF[prefix+'VGS'].to_numpy()
    
    # Get relevant variables
    self.__id265kPre = yPre[-1]
    self.__id265kPost = yPost[-1]
    self.__id265kScaling = (self.__id265kPost-self.__id265kPre)/self.__id265kPre
    self.__id265kScaling2 = (yPost[0]-yPre[0])/yPre[0]

    # Plot
    plt.plot(xPre, yPre, color='black', marker='.', markersize=3, label="Pre Gamma Irradiation")
    plt.plot(xPost, yPost, color='red', marker='.', markersize=3, label="Post Gamma Irradiation")
    plt.ticklabel_format(axis='y', style='sci', scilimits=(0,0))
    plt.xlim([0,2.5])
    plt.ylim([-0.2e9,3.35e9])
    xMin, xMax, yMin, yMax = plt.axis()
    plt.xlabel(r'$\mathregular{V_{GS}}$ [V]')
    plt.ylabel(r'$\mathregular{I_{D}}$ using 265.4k$\mathregular{\Omega}$ [pA]')
    plt.text((xMax-xMin)*0.05+xMin, (yMax-yMin)*0.90+yMin, "ATLAS ITk", fontstyle='italic', fontweight='bold')
    plt.text((xMax-xMin)*0.205+xMin, (yMax-yMin)*0.90+yMin, "Strip", fontstyle='italic')
    plt.text((xMax-xMin)*0.05+xMin, (yMax-yMin)*0.84+yMin, r"{} Batch Sample".format(self.__batchType))
    plt.text((xMax-xMin)*0.05+xMin, (yMax-yMin)*0.78+yMin, r'GaNFET '+str(self.__number))
    plt.legend(loc=(0.04, 0.64), ncol=1, numpoints=5).get_frame().set_linewidth(0.0)
    try:
      if self.__id265kScaling2*100 > 1000:
        plt.text((xMax-xMin)*0.05+xMin, (yMax-yMin)*0.605+yMin, r"$Z_{off}\,\ll\,265.4\,k\Omega$")
      else:
        plt.text((xMax-xMin)*0.05+xMin, (yMax-yMin)*0.605+yMin, r"$Increase\,=\,$"+str(int(self.__id265kScaling2*100))+r"$\,\%\ at\ V_{GS}\,=\,0\,V$")
    except:
      pass
    try:
      plt.savefig('./../data/plots/'+fileName+'.png', format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)
      pass
    except:
      os.system("mkdir ./../data/plots")
      plt.savefig('./../data/plots/'+fileName+'.png', format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)
      pass
    plt.close()

  def plotOnState265kIG(self):
    # Activate ATLAS style
    ampl.use_atlas_style()

    # General variables 
    prefix = 'ganfet '+str(self.__number)+' '
    fileName = 'ganfet'+str(self.__number)+str(self.__batchType).replace("-","")+'OnState265kIG'

    # Get data
    xPre = self.__onState265kPreIrradDF[prefix+'VGS'].to_numpy()
    yPre = self.__onState265kPreIrradDF[prefix+'IG'].to_numpy()
    xPost = self.__onState265kPostIrradDF[prefix+'VGS'].to_numpy()
    yPost = self.__onState265kPostIrradDF[prefix+'IG'].to_numpy()

    # Plot
    plt.plot(xPre, yPre, color='black', marker='.', markersize=3, label="Pre Gamma Irradiation")
    plt.plot(xPost, yPost, color='red', marker='.', markersize=3, label="Post Gamma Irradiation")
    plt.ticklabel_format(axis='y', style='sci', scilimits=(0,0))
    plt.xlim([0,2.5])
    xMin, xMax, yMin, yMax = plt.axis()
    if self.__batchType == 'Production':
      plt.ylim([(yMax-yMin)*0.07+yMin,(yMax-yMin)*1.43+yMin])
    else:
      plt.ylim([(yMax-yMin)*0.07+yMin,(yMax-yMin)*1.33+yMin])
    plt.xlabel(r'$\mathregular{V_{GS}}$ [V]')
    plt.ylabel(r'$\mathregular{I_{G}}$ using 265.4k$\mathregular{\Omega}$ [pA]')
    xMin, xMax, yMin, yMax = plt.axis()
    plt.text((xMax-xMin)*0.05+xMin, (yMax-yMin)*0.90+yMin, "ATLAS ITk", fontstyle='italic', fontweight='bold')
    plt.text((xMax-xMin)*0.205+xMin, (yMax-yMin)*0.90+yMin, "Strip", fontstyle='italic')
    plt.text((xMax-xMin)*0.05+xMin, (yMax-yMin)*0.84+yMin, r"{} Batch Sample".format(self.__batchType))
    plt.text((xMax-xMin)*0.05+xMin, (yMax-yMin)*0.78+yMin, r'GaNFET '+str(self.__number))
    plt.legend(loc=(0.04, 0.64), ncol=1, numpoints=5).get_frame().set_linewidth(0.0)
    try:
      plt.savefig('./../data/plots/'+fileName+'.png', format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)
      pass
    except:
      os.system("mkdir ./../data/plots")
      plt.savefig('./../data/plots/'+fileName+'.png', format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)
      pass
    plt.close()

  def plotOnState265kIS(self):
    # Activate ATLAS style
    ampl.use_atlas_style()

    # General variables 
    prefix = 'ganfet '+str(self.__number)+' '
    fileName = 'ganfet'+str(self.__number)+str(self.__batchType).replace("-","")+'OnState265kIS'

    # Get data
    xPre = self.__onState265kPreIrradDF[prefix+'VGS'].to_numpy()
    yPre = self.__onState265kPreIrradDF[prefix+'IS'].to_numpy()
    xPost = self.__onState265kPostIrradDF[prefix+'VGS'].to_numpy()
    yPost = self.__onState265kPostIrradDF[prefix+'IS'].to_numpy()

    # Plot
    plt.plot(xPre, yPre, color='black', marker='.', markersize=3, label="Pre Gamma Irradiation")
    plt.plot(xPost, yPost, color='red', marker='.', markersize=3, label="Post Gamma Irradiation")
    plt.ticklabel_format(axis='y', style='sci', scilimits=(0,0))
    plt.xlim([0,2.5])
    xMin, xMax, yMin, yMax = plt.axis()
    plt.ylim([(yMax-yMin)*0.07+yMin,(yMax-yMin)*1.05])
    plt.xlabel(r'$\mathregular{V_{GS}}$ [V]')
    plt.ylabel(r'$\mathregular{I_{S}}$ using 265.4k$\mathregular{\Omega}$ [pA]')
    xMin, xMax, yMin, yMax = plt.axis()
    plt.text((xMax-xMin)*0.05+xMin, (yMax-yMin)*0.90+yMin, "ATLAS ITk", fontstyle='italic', fontweight='bold')
    plt.text((xMax-xMin)*0.205+xMin, (yMax-yMin)*0.90+yMin, "Strip", fontstyle='italic')
    plt.text((xMax-xMin)*0.05+xMin, (yMax-yMin)*0.84+yMin, r"{} Batch Sample".format(self.__batchType))
    plt.text((xMax-xMin)*0.05+xMin, (yMax-yMin)*0.78+yMin, r'GaNFET '+str(self.__number))
    plt.legend(loc=(0.04, 0.64), ncol=1, numpoints=5).get_frame().set_linewidth(0.0)
    try:
      plt.savefig('./../data/plots/'+fileName+'.png', format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)
      pass
    except:
      os.system("mkdir ./../data/plots")
      plt.savefig('./../data/plots/'+fileName+'.png', format='png', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".pdf", format='pdf', dpi=200)
      plt.savefig('./../data/plots/'+fileName+".eps", format='eps', dpi=200)
      pass
    plt.close()

  def voltageTh(self):
    try:
      return self.__vThPre, self.__vThPost, self.__vThShift
    except:
      return -99,-99,-99
  def currLeakageIdAt550(self):
    try:
      return self.__idAt550Pre, self.__idAt550Post, self.__idAt550Scaling
    except:
      return -99,-99,-99
  def currLeakageIdAt250(self):
    try:
      return self.__idAt250Pre, self.__idAt250Post, self.__idAt250Scaling
    except:
      return -99,-99,-99
  def curr1GId(self):
    try:
      return self.__id1Gpre, self.__id1Gpost, self.__id1Gscaling
    except:
      return -99,-99,-99
  def curr265kId(self):
    try:
      return self.__id265kPre, self.__id265kPost, self.__id265kScaling
    except:
      return -99,-99,-99
