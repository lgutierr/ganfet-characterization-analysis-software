# GaNFET characterization analysis software

## General information
Software developed for python3 using:
  * ``` openpyxl ```: For reading excel data files
  * ``` numpy ```: For data handling.
  * ``` pandas ```: For data handling.
  * ``` matplotlib ```: For plotting.
  * ``` atlas_mpl_style ```: For ATLAS plotting formatting.

Software takes data from excel file in the format shown in ```dataExample``` folder.

**WARNING:** ```dataExample``` is only an example for format purposes. Do not add data in software folder to avoid accidental commits of large files.

## Run software
```
python3 analysis.py
```
