# General libraries
import time
import os

# Custom libraries
import dataReader as dR
import ganfet
import merger

def analysis(batchType):

  # ~~~ Read all the ganfets selected for gamma irradiation ~~~ #
  print("Reading data files")
  # Start timer
  startTime = time.time()
  # Get raw data
  dataFolder = "/Users/felipegutierrez/Documents/CERN/ATLAS/work/ITk/strips/stave/module/powerboard/GaNFET/characterization/data/"
  if batchType == 'Pre-production':
    preIrradData = os.path.join(dataFolder, "PGA26E19BA_PreIrrad.xlsx")
    postIrradData = os.path.join(dataFolder, "PGA26E19BA_PostIrrad.xlsx")
  elif batchType == 'Production':
    preIrradData = os.path.join(dataFolder, "GaNFETsCharacterizationPreIrrad.xlsx")
    postIrradData = os.path.join(dataFolder, "GaNFETsCharacterizationPostGammaIrrad.xlsx")
  vgsIdRampGaNFET1to20preIrrad = dR.dataReader('vgsIdRamp1to20', preIrradData, 'VgsIdRampData1-20')
  vgsIdRampGaNFET21to40preIrrad = dR.dataReader('vgsIdRamp21to40', preIrradData,'VgsIdRampData21-40')
  vgsIdRampGaNFET41to60preIrrad = dR.dataReader('vgsIdRamp41to60', preIrradData,'VgsIdRampData41-60')
  vgsIdRampGaNFET61to80preIrrad = dR.dataReader('vgsIdRamp61to80', preIrradData, 'VgsIdRampData61-80')
  vgsIdRampGaNFET1to20postIrrad = dR.dataReader('vgsIdRamp1to20', postIrradData, 'VgsIdRampData1-20')
  vgsIdRampGaNFET21to40postIrrad = dR.dataReader('vgsIdRamp21to40', postIrradData, 'VgsIdRampData21-40')
  vgsIdRampGaNFET41to60postIrrad = dR.dataReader('vgsIdRamp41to60', postIrradData, 'VgsIdRampData41-60')
  vgsIdRampGaNFET61to80postIrrad = dR.dataReader('vgsIdRamp61to80', postIrradData,'VgsIdRampData61-80')
  onStateGaNFET1to20preIrrad = dR.dataReader('onState1to20', preIrradData,'OnStateData1-20')
  onStateGaNFET21to40preIrrad = dR.dataReader('onState21to40', preIrradData,'OnStateData21-40')
  onStateGaNFET41to60preIrrad = dR.dataReader('onState41to60', preIrradData, 'OnStateData41-60')
  onStateGaNFET61to80preIrrad = dR.dataReader('onState61to80', preIrradData, 'OnStateData61-80')
  onStateGaNFET1to20postIrrad = dR.dataReader('onState1to20', postIrradData, 'OnStateData1-20')
  onStateGaNFET21to40postIrrad = dR.dataReader('onState21to40', postIrradData, 'OnStateData21-40')
  onStateGaNFET41to60postIrrad = dR.dataReader('onState41to60', postIrradData, 'OnStateData41-60')
  onStateGaNFET61to80postIrrad = dR.dataReader('onState61to80', postIrradData, 'OnStateData61-80')
  offStateGaNFET1to20preIrrad = dR.dataReader('offState1to20', preIrradData, 'OffStateData1-20')
  offStateGaNFET21to40preIrrad = dR.dataReader('offState21to40', preIrradData,'OffStateData21-40')
  offStateGaNFET41to60preIrrad = dR.dataReader('offState41to60', preIrradData, 'OffStateData41-60')
  offStateGaNFET61to80preIrrad = dR.dataReader('offState61to80', preIrradData, 'OffStateData61-80')
  offStateGaNFET1to20postIrrad = dR.dataReader('offState1to20', postIrradData, 'OffStateData1-20')
  offStateGaNFET21to40postIrrad = dR.dataReader('offState21to40', postIrradData, 'OffStateData21-40')
  offStateGaNFET41to60postIrrad = dR.dataReader('offState41to60', postIrradData, 'OffStateData41-60')
  offStateGaNFET61to80postIrrad = dR.dataReader('offState61to80', postIrradData, 'OffStateData61-80')
  # Print time
  print("  Total time: %s seconds" % (time.time()-startTime))

  # ~~~ Merge dataframes ~~~ #
  print("Getting data frames...")
  # Start timer
  startTime = time.time()
  # Combine DFs
  vgsIdRampPreIrrad = vgsIdRampGaNFET1to20preIrrad.DF()[0]
  vgsIdRampPreIrrad = vgsIdRampPreIrrad.join(vgsIdRampGaNFET21to40preIrrad.DF()[0])
  vgsIdRampPreIrrad = vgsIdRampPreIrrad.join(vgsIdRampGaNFET41to60preIrrad.DF()[0])
  vgsIdRampPreIrrad = vgsIdRampPreIrrad.join(vgsIdRampGaNFET61to80preIrrad.DF()[0])
  vgsIdRampPostIrrad = vgsIdRampGaNFET1to20postIrrad.DF()[0]
  vgsIdRampPostIrrad = vgsIdRampPostIrrad.join(vgsIdRampGaNFET21to40postIrrad.DF()[0])
  vgsIdRampPostIrrad = vgsIdRampPostIrrad.join(vgsIdRampGaNFET41to60postIrrad.DF()[0])
  vgsIdRampPostIrrad = vgsIdRampPostIrrad.join(vgsIdRampGaNFET61to80postIrrad.DF()[0])
  onState1GigPreIrrad = onStateGaNFET1to20preIrrad.DF()[0]
  onState1GigPreIrrad = onState1GigPreIrrad.join(onStateGaNFET21to40preIrrad.DF()[0])
  onState1GigPreIrrad = onState1GigPreIrrad.join(onStateGaNFET41to60preIrrad.DF()[0])
  onState1GigPreIrrad = onState1GigPreIrrad.join(onStateGaNFET61to80preIrrad.DF()[0])
  onState1GigPostIrrad = onStateGaNFET1to20postIrrad.DF()[0]
  onState1GigPostIrrad = onState1GigPostIrrad.join(onStateGaNFET21to40postIrrad.DF()[0])
  onState1GigPostIrrad = onState1GigPostIrrad.join(onStateGaNFET41to60postIrrad.DF()[0])
  onState1GigPostIrrad = onState1GigPostIrrad.join(onStateGaNFET61to80postIrrad.DF()[0])
  onState265kPreIrrad = onStateGaNFET1to20preIrrad.DF()[1]
  onState265kPreIrrad = onState265kPreIrrad.join(onStateGaNFET21to40preIrrad.DF()[1])
  onState265kPreIrrad = onState265kPreIrrad.join(onStateGaNFET41to60preIrrad.DF()[1])
  onState265kPreIrrad = onState265kPreIrrad.join(onStateGaNFET61to80preIrrad.DF()[1])
  onState265kPostIrrad = onStateGaNFET1to20postIrrad.DF()[1]
  onState265kPostIrrad = onState265kPostIrrad.join(onStateGaNFET21to40postIrrad.DF()[1])
  onState265kPostIrrad = onState265kPostIrrad.join(onStateGaNFET41to60postIrrad.DF()[1])
  onState265kPostIrrad = onState265kPostIrrad.join(onStateGaNFET61to80postIrrad.DF()[1])
  offStatePreIrrad = offStateGaNFET1to20preIrrad.DF()[0]
  offStatePreIrrad = offStatePreIrrad.join(offStateGaNFET21to40preIrrad.DF()[0])
  offStatePreIrrad = offStatePreIrrad.join(offStateGaNFET41to60preIrrad.DF()[0])
  offStatePreIrrad = offStatePreIrrad.join(offStateGaNFET61to80preIrrad.DF()[0])
  offStatePostIrrad = offStateGaNFET1to20postIrrad.DF()[0]
  offStatePostIrrad = offStatePostIrrad.join(offStateGaNFET21to40postIrrad.DF()[0])
  offStatePostIrrad = offStatePostIrrad.join(offStateGaNFET41to60postIrrad.DF()[0])
  offStatePostIrrad = offStatePostIrrad.join(offStateGaNFET61to80postIrrad.DF()[0])
  # Print time
  print("  Total time: %s seconds" % (time.time()-startTime))

  # ~~~ Define ganfets ~~~ #
  print("Building GaNFETs...")
  # Start timer
  startTime = time.time()
  # Gather ganfets data
  ganfets = []
  for i in range(len(vgsIdRampPreIrrad.columns)):
    if (i % 4) == 0:
      gF = ganfet.ganfet(batchType, i//4 + 1)
      #print("Building GaNFET "+str(gF.num()))
      vgsIdRampColumns = vgsIdRampPreIrrad.columns
      gF.setVgsIdRampPreIrrad(vgsIdRampPreIrrad[[vgsIdRampColumns[i+x] for x in range(4)]])
      gF.setVgsIdRampPostIrrad(vgsIdRampPostIrrad[[vgsIdRampColumns[i+x] for x in range(4)]])
      onStateColumns = onState1GigPreIrrad.columns
      gF.setOnState1GigPreIrrad(onState1GigPreIrrad[[onStateColumns[i+x] for x in range(4)]])
      gF.setOnState1GigPostIrrad(onState1GigPostIrrad[[onStateColumns[i+x] for x in range(4)]])
      gF.setOnState265kPreIrrad(onState265kPreIrrad[[onStateColumns[i+x] for x in range(4)]])
      gF.setOnState265kPostIrrad(onState265kPostIrrad[[onStateColumns[i+x] for x in range(4)]])
      offStateColumns = offStatePreIrrad.columns
      gF.setOffStatePreIrrad(offStatePreIrrad[[offStateColumns[i+x] for x in range(4)]])
      gF.setOffStatePostIrrad(offStatePostIrrad[[offStateColumns[i+x] for x in range(4)]])
      ganfets.append(gF)
  # Print time
  print("  Total time: %s seconds" % (time.time()-startTime))
  
  # ~~~ Plot individual ganfet data ~~~ #
  for fet in ganfets:
    # Print out
    print("Plotting individual GaNFET "+str(fet.num())+" data...")
    # Start timer
    startTime = time.time()
    # Plots
    fet.plotVgsIdRamp()
    fet.plotVgsIdRampVDS()
    fet.plotVgsIdRampVsupply()
    fet.plotOnState1Gig()
    fet.plotOnState1GigIG()
    fet.plotOnState1GigIS()
    fet.plotOnState265k()
    fet.plotOnState265kIG()
    fet.plotOnState265kIS()
    fet.plotOffState()
    fet.plotOffState(True)
    fet.plotOffStateDerivative()
    fet.plotOffStateDerivative(True)
    fet.plotOffStateIG()
    fet.plotOffStateIS()
    # Print plotting time
    print("  Total time: %s seconds" % (time.time()-startTime))
  
  # ~~~ Create merger class ~~~ #
  print("Creating merger...")
  mG = merger.merger(batchType, ganfets)

  # ~~~ Plot merged data ~~~ #
  print("Making merged plots...")
  # Start merger plotting timer
  startTime = time.time()
  mG.plotVgsIdRamp(True)
  mG.plotIdAtVgs0VFromVgsIdRamp(True)
  mG.plotIdShiftAtVgs0VFromVgsIdRamp(True)
  mG.plotIdAtVgs2p5VFromVgsIdRamp(True)
  mG.plotIdShiftAtVgs2p5VFromVgsIdRamp(True)
  mG.plotVoltageThsFromVgsIdRamp(True)
  mG.plotVoltageThsShiftFromVgsIdRamp(True)
  mG.plotVDSFromVgsIdRamp(True)
  mG.plotVsupplyFromVgsIdRamp(True)
  mG.plotOnState1Gig(True)
  mG.plotIdAtVgs0VFromOnState1Gig(True)
  mG.plotIdChangeAtVgs0VFromOnState1Gig(True)
  mG.plotIgFromOnState1Gig(True)
  mG.plotIsFromOnState1Gig(True)
  mG.plotOnState265k(True)
  mG.plotIdAtVgs2p5VFromOnState265k(True)
  mG.plotIdChangeAtVgs2p5VFromOnState265k(True)
  mG.plotIgFromOnState265k(True)
  mG.plotIsFromOnState265k(True)
  mG.plotOffState(True)
  mG.plotOffState(True, True)
  mG.plotOffStateDerivative(True)
  mG.plotOffStateDerivative(True, True)
  mG.plotIdAtVds550VFromOffState(True)
  mG.plotIdChangeAtVds550VFromOffState(True)
  mG.plotIdAtVds250VFromOffState(True)
  mG.plotIdChangeAtVds250VFromOffState(True)
  mG.plotIgFromOffState(True)
  mG.plotIsFromOffState(True)
  
  # Print plotting time
  print("  Total time: %s seconds" % (time.time()-startTime))

if __name__=="__main__":
  print('Pre-production batch plotting')
  analysis('Pre-production')
  print('Production batch plotting')
  analysis('Production')
